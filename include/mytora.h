#pragma once

#include <mymodule.h>

// -- MyTORALoginView -- 

class MyTORALoginView : public MyLoginView
{
	typedef MyLoginView Base;
private:
	wxPropertyGrid* pg_ = nullptr;
	std::string wizard_;
	std::string userid_;
	zqdb::Msg req_;
	std::function<void(int code, const wxString& msg)> cb_;
public:
	MyTORALoginView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
	~MyTORALoginView();

	wxString GetValue(wxString name, const wxString& def = wxEmptyString);

	//改变类型模式
	void SetTypeMode(ZQDB_APP_TYPE type, ZQDB_APP_RUN_MODE mode);
	//返回登录信息是否有效
	bool IsValid();
	//保存登录信息到cfg
	void Save(boost::property_tree::ptree& cfg);

	virtual const char* StartLogin(std::function<void(int code, const wxString& msg)>&& cb)override;
	virtual void CancelLogin(bool logout = true)override;

	void OnResponse(ZQDB_MSG* rsp);
};

// -- MyTORAUserInfoView -- 

class MyTORAUserInfoView : public MyUserViewT<MyTORAUserInfoView>
{
	typedef MyUserViewT<MyTORAUserInfoView> Base;
private:
	wxDataViewCtrl* ctrl_user_list_ = nullptr;
	wxObjectDataPtr<zqdb::HZQDBModel> ctrl_user_list_model_;
	wxDataViewCtrl* ctrl_account_list_ = nullptr;
	wxObjectDataPtr<zqdb::HZQDBListModel> ctrl_account_list_model_;
	wxDataViewCtrl* ctrl_investor_list_ = nullptr;
	wxObjectDataPtr<zqdb::HZQDBListModel> ctrl_investor_list_model_;
public:
	MyTORAUserInfoView(wxWindow *parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

	void OnSkinInfoChanged();
	void OnUserChanged();
};

// -- MyTORAUserOrderView -- 

class MyTORAUserOrderView : public MyUserViewT<MyTORAUserOrderView>
{
	typedef MyUserViewT<MyTORAUserOrderView> Base;
private:
	wxDataViewCtrl* ctrl_list_ = nullptr;
	wxObjectDataPtr<zqdb::HZQDBListModel> ctrl_list_model_;
public:
	MyTORAUserOrderView(wxWindow *parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

	void ShowAll();
	void ClearAll();
	void RefreshAll();

	void OnSkinInfoChanged();
	void OnHandleChanged();
	void OnUserChanged();

protected:
	void OnShowEvent(wxShowEvent& evt);
	void OnSelChanged(wxDataViewEvent &event);
	void OnActivated(wxDataViewEvent &event);
	wxDECLARE_EVENT_TABLE();
};

// -- MyTORAUserTradeView -- 

class MyTORAUserTradeView : public MyUserViewT<MyTORAUserTradeView>
{
	typedef MyUserViewT<MyTORAUserTradeView> Base;
private:
	wxDataViewCtrl* ctrl_list_ = nullptr;
	wxObjectDataPtr<zqdb::HZQDBListModel> ctrl_list_model_;
public:
	MyTORAUserTradeView(wxWindow *parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

	void ShowAll();
	void ClearAll();
	void RefreshAll();

protected:
	void OnShowEvent(wxShowEvent& evt);
	void OnSelChanged(wxDataViewEvent &event);
	void OnActivated(wxDataViewEvent &event);
	wxDECLARE_EVENT_TABLE();
};

// -- MyTORAUserPositionView -- 

class MyTORAUserPositionView : public MyUserViewT<MyTORAUserPositionView>
{
	typedef MyUserViewT<MyTORAUserPositionView> Base;
private:
	wxDataViewCtrl* ctrl_list_ = nullptr;
	wxObjectDataPtr<zqdb::HZQDBListModel> ctrl_list_model_;
public:
	MyTORAUserPositionView(wxWindow *parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

	void ShowAll();
	void ClearAll();
	void RefreshAll();

protected:
	void OnShowEvent(wxShowEvent& evt);
	void OnSelChanged(wxDataViewEvent &event);
	void OnActivated(wxDataViewEvent &event);
	wxDECLARE_EVENT_TABLE();
};

// -- MyTORAMiniView -- 

class MyTORAMiniView : public MyMiniView
{
	typedef MyMiniView Base;
private:
	enum ID_COMMANDS
	{
		// these should be in the same order as Type_XXX elements above
		ID_BOOK_NOTEBOOK = wxID_HIGHEST,
		ID_BOOK_LISTBOOK,
		ID_BOOK_CHOICEBOOK,
		ID_BOOK_TREEBOOK,
		ID_BOOK_TOOLBOOK,
		ID_BOOK_AUINOTEBOOK,
		ID_BOOK_SIMPLEBOOK,
		ID_BOOK_MAX,

		ID_ORIENT_DEFAULT,
		ID_ORIENT_TOP,
		ID_ORIENT_BOTTOM,
		ID_ORIENT_LEFT,
		ID_ORIENT_RIGHT,
		ID_ORIENT_MAX,
		ID_SHOW_IMAGES,
		ID_FIXEDWIDTH,
		ID_MULTI,
		ID_NOPAGETHEME,
		ID_BUTTONBAR,
		ID_HORZ_LAYOUT,
		ID_ADD_PAGE,
		ID_ADD_PAGE_NO_SELECT,
		ID_INSERT_PAGE,
		ID_DELETE_CUR_PAGE,
		ID_DELETE_LAST_PAGE,
		ID_NEXT_PAGE,
		ID_ADD_PAGE_BEFORE,
		ID_ADD_SUB_PAGE,
		ID_CHANGE_SELECTION,
		ID_SET_SELECTION,
		ID_GET_PAGE_SIZE,
		ID_SET_PAGE_SIZE,

#if wxUSE_HELP
		ID_CONTEXT_HELP,
#endif // wxUSE_HELP
		ID_HITTEST
	};
	// Sample setup
	enum BookType
	{
		Type_Notebook,
		Type_Listbook,
		Type_Choicebook,
		Type_Treebook,
		Type_Toolbook,
		Type_AuiNotebook,
		Type_Simplebook,
		Type_Max
	} m_type;
	int m_orient;
	bool m_chkShowImages;
	bool m_fixedWidth;
	bool m_multi;
	bool m_noPageTheme;
	bool m_buttonBar;
	bool m_horzLayout;
	wxBookCtrlBase *m_bookCtrl; 
	MyTORAUserInfoView* info_view_ = nullptr;
	MyTORAUserOrderView* order_view_ = nullptr;
	MyTORAUserTradeView* trade_view_ = nullptr;
	MyTORAUserPositionView* position_view_ = nullptr;
public:
	MyTORAMiniView(wxWindow *parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

	void OnSkinInfoChanged();
	void OnHandleChanged();
	void OnUserChanged();

	void OnNotifyStatus(HZQDB h);
	void OnNotifyAdd(HZQDB h);
	void OnNotifyUpdate(HZQDB h);
protected:
	//void DoGoto(const wxString& view);
	void ShowView(wxWindow* page);

	void OnTimer(wxTimerEvent& event);
	void OnShowEvent(wxShowEvent& evt);
	wxDECLARE_EVENT_TABLE();
};

// -- MyTORAModule --

class MyTORAModule : public MyModule
{
	typedef MyModule Base;
private:	
	size_t max_time_point_ = 0;
	void RefreshBaseInfo();
	void ClearBaseInfo();
	inline bool IsBaseInfoOk() { return max_time_point_ != 0; }
public:
	MyTORAModule(HZQDB h);
	virtual ~MyTORAModule();

	//size_t GetMaxTimePoint() { return max_time_point_; }

	uint32_t GetTradeTime(uint32_t* date = nullptr, uint32_t* tradeday = nullptr);

	wxString GetUserInfo(HZQDB user);

	MyLoginView* NewLoginView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
	MyMiniView* NewMiniView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

	//int ReqModifyPassword(HZQDB user, const char* old_pwd, const char* new_pwd, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0);
	//int ReqModifyAccountPassword(HZQDB account, const char* old_pwd, const char* new_pwd, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0);

	int OrderSend(HZQDB user, HZQDB code, char direction, char offset, char type, double volume, double price, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0);
	int OrderCancel(HZQDB user, HZQDB order, HNMSG* rsp = nullptr, size_t timeout = 0, size_t flags = 0);
	int OrderClose(HZQDB user, HZQDB position, char type, double volume, double price, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0);

	int ReqQryMaxOrderVolume(HZQDB user, HZQDB code, char direction, char offset, char type, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0);

	void OnTimer();

	void OnNotifyStatus(HZQDB h);
	void OnNotifyAdd(HZQDB h);
	void OnNotifyUpdate(HZQDB h);

	//void OnNetStatus(HNNODE h, NET_NODE_STATUS status);
	int OnNetMsg(zqdb::Msg& msg);
};

