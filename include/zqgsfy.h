#pragma once

#ifndef _H_ZQGSFY_H_
#define _H_ZQGSFY_H_

#include <zqdb.h>

#ifndef GSFY_API_EXPORT
#define GSFY_API_EXPORT
#endif//GSFY_API_EXPORT

#if defined(__cplusplus)
extern "C" {
#endif//

#pragma pack(push, 1)

	///用户信息
	struct tagGSFYUserInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Status; //用户状态 USER_STATUS_OFFLINE

		uint32_t TradingDay; //交易日

		char UserName[81]; //客户姓名
		int32_t SysType; //系统类型:4AB股交易系统 7融资融券交易 9个股期权交易 99其他交易系统
		char NodeID[31]; //节点编号,如果结果多条,格式:1,2
		char Token[129]; //登录令牌
		byte SysTestFlag; //测试状态：1测试状态，0正常交易状态
		char LastEntrustWay[31];//上次登录委托方式
		char LastNote[129];//上次登陆IP,MAC
		byte InnerFlag;//内部账号标志 0:非内部账号,1:内部账号
		char Token2[129];//服务端访问A5柜台的用，app不需要交互，所以协议向前做了兼容
	};
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, BROKER);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, USER);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, STATUS);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, TradingDay);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, UserName);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, SysType);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, NodeID);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, Token);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, SysTestFlag);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, LastEntrustWay);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, LastNote);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, InnerFlag);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, USER, Token2);
	DECLARE_MDB_FIELD_TABLE(GSFY_API_EXPORT, GSFY, USER, 13);
	DECLARE_MDB_FIELD_NAME_TABLE(GSFY_API_EXPORT, GSFY, USER);

	///投资者信息
	struct tagGSFYInvestorInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Investor[MAX_CODE_LENGTH + 1]; //投资者代码
		char InvestorName[MAX_NAME_LENGTH + 1]; //投资者名称
		//
		char Exchange[MAX_EXCHANGE_LENGTH + 1]; //交易所
	};
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, INVESTOR, BROKER);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, INVESTOR, USER);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, INVESTOR, INVESTOR);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, INVESTOR, INVESTOR_NAME);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, INVESTOR, Exchange);
	//
	DECLARE_MDB_FIELD_TABLE(GSFY_API_EXPORT, GSFY, INVESTOR, 5);
	DECLARE_MDB_FIELD_NAME_TABLE(GSFY_API_EXPORT, GSFY, INVESTOR);

	///资金账户信息
	struct tagGSFYAccountInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Account[MAX_CODE_LENGTH + 1]; //资金账户
		//
		char CurrencyID;//币种
		int32_t SysType; //系统类型:4AB股交易系统 7融资融券交易 9个股期权交易 99其他交易系统
		char NodeID[31]; //节点编号,如果结果多条,格式:1,2
		byte Status;//账户状态 0正常#
		byte AcctType;//账户类别 0容错户，1保证金托管，2信用账户，5期权账户#
		byte DeposType;//存管类别# 
		byte OrgFlag;//机构标志#
		uint32_t BeginDate;//开户日期#
		uint32_t EndDate;//销户日期#
		char CustName[81];//客户姓名#
		byte AcctFlag;//主账户标志 1主账户
		//
		double FundBal; //账户余额
		double TotalAssets; //总资产
		double JJVal; //基金市值
		double ZQVal; //证券市值#
		double OtherVal; //其他资产#
		double FundAvis; //可取资金
		double FundAvail; //可用资金
		double Interest; //待结利息#
		double InterestJS; //利息积数#
		double DJVal; //冻结资金
	};
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, BROKER);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, USER);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, ACCOUNT);
	//
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, CurrencyID);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, SysType);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, NodeID);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, Status);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, AcctType);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, DeposType);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, OrgFlag);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, BeginDate);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, EndDate);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, CustName);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, AcctFlag);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, FundBal);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, TotalAssets);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, JJVal);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, ZQVal);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, OtherVal);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, FundAvis);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, FundAvail);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, Interest);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, InterestJS);
	DECLARE_MDB_FIELD(GSFY_API_EXPORT, GSFY, ACCOUNT, DJVal);
	DECLARE_MDB_FIELD_TABLE(GSFY_API_EXPORT, GSFY, ACCOUNT, 24);
	DECLARE_MDB_FIELD_NAME_TABLE(GSFY_API_EXPORT, GSFY, ACCOUNT);

#pragma pack(pop)

#if defined(__cplusplus)
}

#ifdef STR_GSFY_MODULE

	inline const char STATUS_GSFY2ZQDB(const char status) {
		/*switch (status)
		{
		case GSFYSTOCKAPI::GSFY_TSTP_OST_Cached:
			return ORDER_STATUS_NOTTRADED;
			break;
		case GSFYSTOCKAPI::GSFY_TSTP_OST_Unknown:
			return ORDER_STATUS_SUBMITTING;
			break;
		case GSFYSTOCKAPI::GSFY_TSTP_OST_Accepted:
			return ORDER_STATUS_NOTTRADED;
			break;
		case GSFYSTOCKAPI::GSFY_TSTP_OST_PartTraded:
			return ORDER_STATUS_PARTTRADED;
			break;
		case GSFYSTOCKAPI::GSFY_TSTP_OST_AllTraded:
			return ORDER_STATUS_ALLTRADED;
			break;
		case GSFYSTOCKAPI::GSFY_TSTP_OST_PartTradeCanceled:
			return ORDER_STATUS_CANCELLED;
			break;
		case GSFYSTOCKAPI::GSFY_TSTP_OST_AllCanceled:
			return ORDER_STATUS_CANCELLED;
			break;
		case GSFYSTOCKAPI::GSFY_TSTP_OST_Rejected:
			return ORDER_STATUS_REJECTED;
			break;
		case GSFYSTOCKAPI::GSFY_TSTP_OST_SendTradeEngine:
			return ORDER_STATUS_SUBMITTING;
			break;
			break;
		default:
			break;
		}*/
		return 0;
	}

	inline const char DIRECTION_ZQDB2GSFY(const char direction) {
		/*switch (direction)
		{
		case DIRECTION_LONG:
			return GSFYSTOCKAPI::GSFY_TSTP_D_Buy;
		case DIRECTION_SHORT:
			return GSFYSTOCKAPI::GSFY_TSTP_D_Sell;
		}*/
		return 0;
	}
	inline const char DIRECTION_GSFY2ZQDB(const char direction) {
		/*switch (direction)
		{
		case GSFYSTOCKAPI::GSFY_TSTP_D_Buy:
			return DIRECTION_LONG;
		case GSFYSTOCKAPI::GSFY_TSTP_D_Sell:
			return DIRECTION_SHORT;
		}*/
		return 0;
	}

	inline const char OFFSET_GSFY2ZQDB(const char direction) {
		/*switch (direction)
		{
		case GSFYSTOCKAPI::GSFY_TSTP_D_Buy:
			return OFFSET_OPEN;
		case GSFYSTOCKAPI::GSFY_TSTP_D_Sell:
			return OFFSET_CLOSE;
		}*/
		return 0;
	}

	inline const char OFFSET_ZQDB2GSFY(const char offset) {
		/*switch (offset)
		{
		case OFFSET_OPEN:
			return GSFYSTOCKAPI::GSFY_TSTP_D_Buy;
		case OFFSET_CLOSE:
			return GSFYSTOCKAPI::GSFY_TSTP_D_Sell;
		}*/
		return 0;
	}

#endif//

#endif//

#endif//_H_ZQGSFY_H_