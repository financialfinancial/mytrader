#pragma once

#ifndef _H_ZQCTP_H_
#define _H_ZQCTP_H_

#include <zqdb.h>

#ifndef CTP_API_EXPORT
#define CTP_API_EXPORT
#endif//CTP_API_EXPORT

#if defined(__cplusplus)
extern "C" {
#endif//

#pragma pack(push, 1)

	typedef struct tagCTPCodeInfo
	{
		char Exchange[MAX_EXCHANGE_LENGTH + 1];
		char Product[MAX_PRODUCT_LENGTH + 1];
		char Code[MAX_CODE_LENGTH + 1];
		char Name[MAX_NAME_LENGTH + 1];
		char TradeCode[MAX_CODE_LENGTH + 1]; //交易代码
		uint32_t Date; // YYYYMMDD
		uint32_t Time; // HHMMSSsss
		char Status; //市场交易状态
		char EnterReason; //进入交易状态
		double PriceTick; //最小变动价位
		double YClose; //昨收
		double YSettle; //昨结
		double Settle; //结算价
		double Upper; //涨停板价
		double Lower; //跌停板价
		double Open;
		double High;
		double Low;
		double Close; // 交易价格，当然也是最新价，单位：元
		double Amount; // 总额
		double Volume; // 总成交量，单位：最小单位(股，张等)
		double Bid1; //申买价
		double Bid2; //申买价
		double Bid3; //申买价
		double Bid4; //申买价
		double Bid5; //申买价
		double BidV1; //申买量
		double BidV2; //申买量
		double BidV3; //申买量
		double BidV4; //申买量
		double BidV5; //申买量
		double Ask1; //申卖价
		double Ask2; //申卖价
		double Ask3; //申卖价
		double Ask4; //申卖价
		double Ask5; //申卖价
		double AskV1; //申卖量
		double AskV2; //申卖量
		double AskV3; //申卖量
		double AskV4; //申卖量
		double AskV5; //申卖量

		///昨持仓量
		double PreOpenInterest; 
		///持仓量
		double OpenInterest; 
		
		///昨虚实度
		double	PreDelta;
		///今虚实度
		double	CurrDelta;
		///当日均价
		//double	AveragePrice;

		///交割年份
		uint32_t	DeliveryYear;
		///交割月
		uint32_t	DeliveryMonth;
		///市价单最大下单量
		double	MaxMarketOrderVolume;
		///市价单最小下单量
		double	MinMarketOrderVolume;
		///限价单最大下单量
		double	MaxLimitOrderVolume;
		///限价单最小下单量
		double	MinLimitOrderVolume;
		///合约数量乘数
		uint32_t	VolumeMultiple;
		///创建日
		uint32_t	CreateDate;
		///上市日
		uint32_t	OpenDate;
		///到期日
		uint32_t	ExpireDate;
		///开始交割日
		uint32_t	StartDelivDate;
		///结束交割日
		uint32_t	EndDelivDate;
		///合约生命周期状态
		char	InstLifePhase;
		///当前是否交易
		bool	IsTrading;
		///持仓类型
		char	PositionType;
		///持仓日期类型
		char	PositionDateType;
		///多头保证金率
		double	LongMarginRatio;
		///空头保证金率
		double	ShortMarginRatio;
		///是否使用大额单边保证金算法
		char	MaxMarginSideAlgorithm;
		///基础商品代码
		char	UnderlyingInstrID[31];
		///执行价
		double	StrikePrice;
		///期权类型
		char	OptionsType;
		///合约基础商品乘数
		double	UnderlyingMultiple;
		///组合类型
		char	CombinationType;

	}CTPCODEINFO, *PCTPCODEINFO;
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, EXCHANGE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, PRODUCT);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, CODE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, NAME);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, TRADE_CODE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, DATE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, TIME);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, STATUS);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ENTER_REASON);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, PRICETICK);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, YCLOSE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, YSETTLE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, SETTLE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, UPPER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, LOWER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, OPEN);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, HIGH);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, LOW);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, CLOSE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, AMOUNT);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, VOLUME); 
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BID1);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BID2);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BID3);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BID4);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BID5);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BIDV1);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BIDV2);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BIDV3);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BIDV4);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, BIDV5);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASK1);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASK2);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASK3);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASK4);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASK5);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASKV1);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASKV2);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASKV3);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASKV4);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ASKV5);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, PreOpenInterest);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, OpenInterest);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, PreDelta);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, CurrDelta);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, DeliveryYear);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, DeliveryMonth);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, MaxMarketOrderVolume);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, MinMarketOrderVolume);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, MaxLimitOrderVolume);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, MinLimitOrderVolume);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, VolumeMultiple);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, CreateDate);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, OpenDate);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ExpireDate);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, StartDelivDate);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, EndDelivDate);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, InstLifePhase);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, IsTrading);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, PositionType);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, PositionDateType);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, LongMarginRatio);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, ShortMarginRatio);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, MaxMarginSideAlgorithm);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, UnderlyingInstrID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, StrikePrice);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, OptionsType);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, UnderlyingMultiple);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, CODE, CombinationType);
	DECLARE_MDB_FIELD_TABLE(CTP_API_EXPORT, CTP, CODE, 69);
	DECLARE_MDB_FIELD_NAME_TABLE(CTP_API_EXPORT, CTP, CODE);

	//明细结构
	typedef struct tagCTPTick
	{
		uint32_t Date; // YYYYMMDD
		uint32_t Time; // HHMMSSsss
		uint32_t Flags; //
		double Close; // 交易价格，当然也是最新价，单位：元，放大PriceRate倍
		double Volume; // 成交量(现量)，单位：最小单位(股，张等)
		double OpenInterest; //持仓量
	} CTPTICK, *PCTPTICK;
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, TICK, DATE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, TICK, TIME);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, TICK, FLAGS);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, TICK, CLOSE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, TICK, VOLUME);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, TICK, OpenInterest);
	DECLARE_MDB_FIELD_TABLE(CTP_API_EXPORT, CTP, TICK, 6);
	DECLARE_MDB_FIELD_NAME_TABLE(CTP_API_EXPORT, CTP, TICK);

	typedef struct tagCTPHistory
	{
		uint32_t Date; // YYYYMMDD
		uint32_t Time; // HHMMSS
		double Open; // 开盘价，单位：元(针对大盘指数表示开盘指数)，放大PriceRate倍
		double High; // 最高价，单位：元(针对大盘指数表示最高指数)，放大PriceRate倍
		double Low; // 最低价，单位：元(针对大盘指数表示最低指数)，放大PriceRate倍
		double Close; // 收盘价，单位：元(针对大盘指数表示收盘指数)，放大PriceRate倍
		double Amount; // 成交金额，单位：元
		double Volume; // 成交量，单位：最小单位(如股、张等)
		double OpenInterest; //持仓量
	} CTPKDATA, *PCTPKDATA, CTPBAR, *PCTPBAR;
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, DATE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, TIME);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, OPEN);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, HIGH);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, LOW);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, CLOSE);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, AMOUNT);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, VOLUME);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, KDATA, OpenInterest);
	DECLARE_MDB_FIELD_TABLE(CTP_API_EXPORT, CTP, KDATA, 9);
	DECLARE_MDB_FIELD_NAME_TABLE(CTP_API_EXPORT, CTP, KDATA);

	///用户信息
	struct tagCTPUserInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Status; //用户状态 USER_STATUS_OFFLINE

		///交易日
		char	TradingDay[9];
		///登录成功时间
		char	LoginTime[9];
		///交易系统名称
		char	SystemName[41];
		///前置编号
		int64_t	FrontID;
		///会话编号
		int64_t	SessionID;
		///最大报单引用
		char	MaxOrderRef[13];
	};
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, BROKER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, USER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, STATUS);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, TradingDay);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, LoginTime);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, SystemName);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, FrontID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, SessionID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, USER, MaxOrderRef);
	DECLARE_MDB_FIELD_TABLE(CTP_API_EXPORT, CTP, USER, 9);
	DECLARE_MDB_FIELD_NAME_TABLE(CTP_API_EXPORT, CTP, USER);

	///投资者信息
	struct tagCTPInvestorInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Investor[MAX_CODE_LENGTH + 1]; //投资者代码
		char InvestorName[MAX_NAME_LENGTH + 1]; //投资者名称

		///投资者分组代码
		char	InvestorGroupID[13];
		///证件类型
		char	IdentifiedCardType;
		///证件号码
		char	IdentifiedCardNo[51];
		///是否活跃
		uint8_t	IsActive;
		///联系电话
		char	Telephone[41];
		///通讯地址
		char	Address[101];
		///开户日期
		char	OpenDate[9];
		///手机
		char	Mobile[41];
		///手续费率模板代码
		char	CommModelID[13];
		///保证金率模板代码
		char	MarginModelID[13];
	};
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, BROKER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, USER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, INVESTOR);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, INVESTOR_NAME);
	//
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, InvestorGroupID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, IdentifiedCardType);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, IdentifiedCardNo);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, IsActive);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, Telephone);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, Address);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, OpenDate);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, Mobile);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, CommModelID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, INVESTOR, MarginModelID);
	DECLARE_MDB_FIELD_TABLE(CTP_API_EXPORT, CTP, INVESTOR, 14);
	DECLARE_MDB_FIELD_NAME_TABLE(CTP_API_EXPORT, CTP, INVESTOR);

	///资金账户信息
	struct tagCTPAccountInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Account[MAX_CODE_LENGTH + 1]; //投资者帐号

		///上次质押金额
		double	PreMortgage;
		///上次信用额度
		double	PreCredit;
		///上次存款额
		double	PreDeposit;
		///上次结算准备金
		double	PreBalance;
		///上次占用的保证金
		double	PreMargin;
		///利息基数
		double	InterestBase;
		///利息收入
		double	Interest;
		///入金金额
		double	Deposit;
		///出金金额
		double	Withdraw;
		///冻结的保证金
		double	FrozenMargin;
		///冻结的资金
		double	FrozenCash;
		///冻结的手续费
		double	FrozenCommission;
		///当前保证金总额
		double	CurrMargin;
		///资金差额
		double	CashIn;
		///手续费
		double	Commission;
		///平仓盈亏
		double	CloseProfit;
		///持仓盈亏
		double	PositionProfit;
		///期货结算准备金
		double	Balance;
		///可用资金
		double	Available;
		///可取资金
		double	WithdrawQuota;
		///基本准备金
		double	Reserve;
		///交易日
		char	TradingDay[9];
		///结算编号
		uint64_t	SettlementID;
		///信用额度
		double	Credit;
		///质押金额
		double	Mortgage;
		///交易所保证金
		double	ExchangeMargin;
		///投资者交割保证金
		double	DeliveryMargin;
		///交易所交割保证金
		double	ExchangeDeliveryMargin;
		///保底期货结算准备金
		double	ReserveBalance;
		///币种代码
		char	CurrencyID[4];
		///上次货币质入金额
		double	PreFundMortgageIn;
		///上次货币质出金额
		double	PreFundMortgageOut;
		///货币质入金额
		double	FundMortgageIn;
		///货币质出金额
		double	FundMortgageOut;
		///货币质押余额
		double	FundMortgageAvailable;
		///可质押货币金额
		double	MortgageableFund;
		///特殊产品占用保证金
		double	SpecProductMargin;
		///特殊产品冻结保证金
		double	SpecProductFrozenMargin;
		///特殊产品手续费
		double	SpecProductCommission;
		///特殊产品冻结手续费
		double	SpecProductFrozenCommission;
		///特殊产品持仓盈亏
		double	SpecProductPositionProfit;
		///特殊产品平仓盈亏
		double	SpecProductCloseProfit;
		///根据持仓盈亏算法计算的特殊产品持仓盈亏
		double	SpecProductPositionProfitByAlg;
		///特殊产品交易所保证金
		double	SpecProductExchangeMargin;
		///业务类型
		char	BizType;
		///延时换汇冻结金额
		double	FrozenSwap;
		///剩余换汇额度
		double	RemainSwap;
	};
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, BROKER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, USER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, ACCOUNT);
	//
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, PreMortgage);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, PreCredit);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, PreDeposit);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, PreBalance);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, PreMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, InterestBase);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Interest);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Deposit);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Withdraw);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, FrozenMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, FrozenCash);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, FrozenCommission);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, CurrMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, CashIn);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Commission);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, CloseProfit);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, PositionProfit);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Balance);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Available);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, WithdrawQuota);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Reserve);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, TradingDay);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, SettlementID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Credit);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, Mortgage);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, ExchangeMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, DeliveryMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, ExchangeDeliveryMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, ReserveBalance);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, CurrencyID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, PreFundMortgageIn);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, PreFundMortgageOut);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, FundMortgageAvailable);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, MortgageableFund);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, SpecProductMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, SpecProductFrozenMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, SpecProductCommission);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, SpecProductPositionProfit);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, SpecProductCloseProfit);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, SpecProductPositionProfitByAlg);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, SpecProductExchangeMargin);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, BizType);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, FrozenSwap);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, ACCOUNT, RemainSwap);
	DECLARE_MDB_FIELD_TABLE(CTP_API_EXPORT, CTP, ACCOUNT, 48);
	DECLARE_MDB_FIELD_NAME_TABLE(CTP_API_EXPORT, CTP, ACCOUNT);

	#define STR_CTP_TABLE_COMMISSIONRATE "COMMISSIONRATE"

	struct tagCommissionRateInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Investor[MAX_CODE_LENGTH + 1]; //投资者代码

		///合约代码
		char	InstrumentID[31];
		///投资者范围
		char	InvestorRange;
		///开仓手续费率
		double	OpenRatioByMoney;
		///开仓手续费
		double	OpenRatioByVolume;
		///平仓手续费率
		double	CloseRatioByMoney;
		///平仓手续费
		double	CloseRatioByVolume;
		///平今手续费率
		double	CloseTodayRatioByMoney;
		///平今手续费
		double	CloseTodayRatioByVolume;
		///交易所代码
		char	ExchangeID[9];
		///业务类型
		char	BizType;
		///投资单元代码
		char	InvestUnitID[17];
	};
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, BROKER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, USER);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, INVESTOR);
	//
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, InstrumentID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, InvestorRange);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, OpenRatioByMoney);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, OpenRatioByVolume);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, CloseRatioByMoney);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, CloseRatioByVolume);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, CloseTodayRatioByMoney);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, CloseTodayRatioByVolume);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, ExchangeID);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, BizType);
	DECLARE_MDB_FIELD(CTP_API_EXPORT, CTP, COMMISSIONRATE, InvestUnitID);
	DECLARE_MDB_FIELD_TABLE(CTP_API_EXPORT, CTP, COMMISSIONRATE, 14);
	DECLARE_MDB_FIELD_NAME_TABLE(CTP_API_EXPORT, CTP, COMMISSIONRATE);

#pragma pack(pop)

#ifdef STR_CTP_MODULE

#include <ThostFtdcTraderApi.h>
#include <ThostFtdcMdApi.h>

inline const char STATUS_CTP2VT(const char status) {
	switch (status)
	{
	case THOST_FTDC_OST_AllTraded:
		return ORDER_STATUS_ALLTRADED;
		break;
	case THOST_FTDC_OST_PartTradedQueueing:
		return ORDER_STATUS_PARTTRADED;
		break;
	case THOST_FTDC_OST_PartTradedNotQueueing:
		return ORDER_STATUS_PARTTRADED;
		break;
	case THOST_FTDC_OST_NoTradeQueueing:
		return ORDER_STATUS_NOTTRADED;
		break;
	case THOST_FTDC_OST_NoTradeNotQueueing:
		return ORDER_STATUS_NOTTRADED;
		break;
	case THOST_FTDC_OST_Canceled:
		return ORDER_STATUS_CANCELLED;
		break;
	case THOST_FTDC_OST_Unknown:
		return ORDER_STATUS_SUBMITTING;
		break;
	case THOST_FTDC_OST_NotTouched:
		return ORDER_STATUS_SUBMITTING;
		break;
	case THOST_FTDC_OST_Touched:
		return ORDER_STATUS_SUBMITTING;
		break;
		break;
	default:
		break;
	}
	return 0;
}

inline const char DIRECTION_VT2CTP(const char direction) {
	switch (direction)
	{
	case DIRECTION_LONG:
		return THOST_FTDC_D_Buy;
	case DIRECTION_SHORT:
		return THOST_FTDC_D_Sell;
	}
	return 0;
}
inline const char DIRECTION_CTP2VT(const char direction) {
	switch (direction)
	{
	case THOST_FTDC_D_Buy:
		return DIRECTION_LONG;
	case THOST_FTDC_D_Sell:
		return DIRECTION_SHORT;
	}
	return 0;
}
inline const char PDIRECTION_VT2CTP(const char direction) {
	switch (direction)
	{
	case DIRECTION_LONG:
		return THOST_FTDC_PD_Long;
	case DIRECTION_SHORT:
		return THOST_FTDC_PD_Short;
	case DIRECTION_NET:
		return THOST_FTDC_PD_Net;
	}
	return 0;
}
inline const char PDIRECTION_CTP2VT(const char direction) {
	switch (direction)
	{
	case THOST_FTDC_PD_Long:
		return DIRECTION_LONG;
	case THOST_FTDC_PD_Short:
		return DIRECTION_SHORT;
	case THOST_FTDC_PD_Net:
		return DIRECTION_NET;
	}
	return 0;
}

inline const char ORDERTYPE_VT2CTP(const char type) {
	switch (type)
	{
	case ORDER_LIMIT:
		return THOST_FTDC_OPT_LimitPrice;
	case ORDER_MARKET:
		return THOST_FTDC_OPT_AnyPrice;
	default:
		break;
	}
	return 0;
}
inline const char ORDERTYPE_CTP2VT(const char type, const char tc, const char vc) {
	switch (type)
	{
	case THOST_FTDC_OPT_LimitPrice: {
		if (tc == THOST_FTDC_TC_IOC) {
			if (vc == THOST_FTDC_VC_AV) {
				return ORDER_FAK;
			}
			else if (vc == THOST_FTDC_VC_CV) {
				return ORDER_FOK;
			}
		}
		return ORDER_LIMIT;
	} break;
	case THOST_FTDC_OPT_AnyPrice: {
		return ORDER_MARKET;
	} break;
	default:
		break;
	}
	return ORDER_MARKET;
}

inline const char OFFSET_VT2CTP(const char offset) {
	switch (offset)
	{
	case OFFSET_OPEN:
		return THOST_FTDC_OF_Open;
	case OFFSET_CLOSE:
		return THOST_FTDC_OFEN_Close;
	case OFFSET_CLOSETODAY:
		return THOST_FTDC_OFEN_CloseToday;
	case OFFSET_CLOSEYESTERDAY:
		return THOST_FTDC_OFEN_CloseYesterday;
	default:
		break;
	}
	return 0;
}
inline const char OFFSET_CTP2VT(const char offset) {
	switch (offset)
	{
	case THOST_FTDC_OF_Open:
		return OFFSET_OPEN;
	case THOST_FTDC_OFEN_Close:
		return OFFSET_CLOSE;
	case THOST_FTDC_OFEN_CloseToday:
		return OFFSET_CLOSETODAY;
	case THOST_FTDC_OFEN_CloseYesterday:
		return OFFSET_CLOSEYESTERDAY;
	default:
		break;
	}
	return 0;
}

// EXCHANGE_CTP2VT = {
//     "CFFEX": Exchange.CFFEX,
//     "SHFE": Exchange.SHFE,
//     "CZCE": Exchange.CZCE,
//     "DCE": Exchange.DCE,
//     "INE": Exchange.INE
// }

//inline const char PRODUCT_CTP2VT(const char type) {
//	switch (type)
//	{
//	case THOST_FTDC_PC_Futures:
//		return PRODUCT_FUTURES;
//	case THOST_FTDC_PC_Options:
//		return PRODUCT_OPTION;
//	case THOST_FTDC_PC_Combination:
//		return PRODUCT_SPREAD;
//	case THOST_FTDC_PC_Spot:
//		return PRODUCT_SPOT;
//	case THOST_FTDC_PC_EFP:
//		return PRODUCT_SPOT;
//	case THOST_FTDC_PC_SpotOption:
//		return PRODUCT_OPTION;
//#ifdef CTP_SOOPT
//	case THOST_FTDC_PC_ETFOption:
//		return PRODUCT_OPTION;
//	case THOST_FTDC_PC_Stock:
//		return PRODUCT_STOCK;
//#endif//
//	default:
//		break;
//	}
//	return 0;
//}

inline const char OPTIONTYPE_CTP2VT(const char type) {
	switch (type)
	{
	case THOST_FTDC_CP_CallOptions:
		return OPTION_CALL;
	case THOST_FTDC_CP_PutOptions:
		return OPTION_PUT;
	default:
		break;
	}
	return 0;
}

#endif//

#if defined(__cplusplus)
}

#endif//

#endif//_H_ZQCTP_H_