#pragma once

#ifndef _H_ZQDB_MODULE_H_
#define _H_ZQDB_MODULE_H_

#include "zqdbif.h"

#if defined(LIB_ZQDB_MODULE_API) && defined(WIN32)
#ifdef LIB_ZQDB_MODULE_API_EXPORT
#define ZQDB_MODULE_API_EXPORT __declspec(dllexport)
#else
#define ZQDB_MODULE_API_EXPORT __declspec(dllimport)
#endif
#else
#define ZQDB_MODULE_API_EXPORT 
#endif

#if defined(__cplusplus)
extern "C" {
#endif//

#pragma pack(push, 1)

typedef uint32_t(*fnTradingDay)();
typedef void(*fnNotify)(ZQDB_MSG* msg);
typedef int (*fnRequest)(ZQDB_MSG* msg);
typedef int(*fnNowData)(HZQDB h, PERIODTYPE cycle, void* data); //最新数据
typedef int(*fnMergeData)(HZQDB h, PERIODTYPE cycle, void* data, const KDATA* base); //合并数据
typedef int(*fnInitData)(HZQDB h, PERIODTYPE cycle, ZQDB_MSG* msg); //初始化数据
typedef int(*fnBuildData)(HZQDB h, PERIODTYPE cycle, ZQDB_MSG* msg);
typedef bool(*fnExistData)(HZQDB h, PERIODTYPE cycle);
typedef int(*fnSaveData)(HZQDB h, PERIODTYPE cycle, ZQDB_MSG* msg); //保存数据
typedef int(*fnSaveClose)(HZQDB h, PERIODTYPE cycle); //收盘保存数据
typedef size_t (*fnPlayRecordData)(const tagZQDBRecordHead* head, const tagZQDBRecordBuf* data, uint8_t count); //返回速度倍数延迟，0即马上播放下一个数据
typedef void(*fnRefresh)(HZQDB h, ssize_t millis); //刷新数据，millis指定刷新毫秒间隔，0表示只刷新一次， 小于0表示停止刷新

typedef struct tagModuleInfoEx
{
	char Code[MAX_CODE_LENGTH + 1]; //模块标识（ctp）
	char Name[MAX_NAME_LENGTH + 1]; //模块名称，模块名（ctp）
	//char Desc[MAX_CONTENT_LENGTH + 1]; //模块描述
	uint32_t flags; //模块标志
	//以上字段和tagModuleInfo保持一致，下面是模块扩展字段
	//market
	bool sub_all; //[out] //全市场订阅
	//trade
	//module
	uint32_t tradingday; //[out] 最新交易日
	uint32_t closeday; //[out] 最新收盘日
	//zqdb
	ZQDB_NOTIFY_CB notify_cb; //[out]
	//msg
	fnNotify notify; //[out]
	fnRequest request; //[out]
	//
	fnNowData now_data; //[out]
	fnMergeData merge_data; //[out]
	fnInitData init_data; //[out]
	fnBuildData build_data; //[out]
	fnExistData exist_data; //[out]
	fnSaveData save_data; //[out]
	fnSaveClose save_close; //[out]
	fnPlayRecordData play_record_data; //[out]
	//
	fnRefresh refresh; //[out]
}MODULEINFOEX, *PMODULEINFOEX;

ZQDB_API_EXPORT HZQDB ZQDBRegisterModule(MODULEINFOEX* info
	, const char* markets[], size_t market_count, const char* trades[], size_t trade_count);

typedef bool (*fnZQDBModuleStart)(const char* xml, int xmlflag);
typedef void(*fnZQDBModuleRun)();
typedef void (*fnZQDBModuleStop)(size_t flags);

ZQDB_MODULE_API_EXPORT bool ZQDBModuleInit(const char* xml, int xmlflag);
ZQDB_MODULE_API_EXPORT bool ZQDBModuleStart();
ZQDB_MODULE_API_EXPORT void ZQDBModuleRun();
ZQDB_MODULE_API_EXPORT void ZQDBModuleStop(size_t flags);

#pragma pack(pop)

#if defined(__cplusplus)
}

#endif//

#endif//_H_ZQDB_MODULE_H_