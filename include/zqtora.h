#pragma once

#ifndef _H_ZQTORA_H_
#define _H_ZQTORA_H_

#include <zqdb.h>

#ifndef TORA_API_EXPORT
#define TORA_API_EXPORT
#endif//TORA_API_EXPORT

#if defined(__cplusplus)
extern "C" {
#endif//

#pragma pack(push, 1)

	///用户信息
	struct tagTORAUserInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Status; //用户状态 USER_STATUS_OFFLINE

		///前置编号
		int64_t FrontID;

		///会话编号
		int64_t	SessionID;

		///最大报单引用
		int64_t	MaxOrderRef;

		///私有流长度
		int32_t	PrivateFlowCount;

		///公有流长度
		int32_t	PublicFlowCount;

		///登录时间
		char	LoginTime[9];

		///交易系统名称
		char	SystemName[41];

		///交易日
		char	TradingDay[9];

		///用户名称
		char	UserName[81];

		///用户类型
		char	UserType;

		///报单流控
		int32_t	OrderInsertCommFlux;

		///撤单流控
		int32_t	OrderActionCommFlux;

		///密码到期日期
		char	PasswordExpiryDate[9];

		///是否需要改密
		int32_t	NeedUpdatePassword;

		///认证序列号
		char	CertSerial[129];

		///内网IP地址
		char	InnerIPAddress[16];

		///外网IP地址
		char	OuterIPAddress[16];

		///Mac地址
		char	MacAddress[21];
	};
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, BROKER);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, USER);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, STATUS);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, FrontID);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, SessionID);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, MaxOrderRef);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, PrivateFlowCount);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, PublicFlowCount);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, LoginTime);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, SystemName);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, TradingDay);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, UserName);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, UserType);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, OrderInsertCommFlux);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, OrderActionCommFlux);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, PasswordExpiryDate);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, NeedUpdatePassword);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, CertSerial);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, InnerIPAddress);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, OuterIPAddress);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, USER, MacAddress);
	DECLARE_MDB_FIELD_TABLE(TORA_API_EXPORT, TORA, USER, 21);
	DECLARE_MDB_FIELD_NAME_TABLE(TORA_API_EXPORT, TORA, USER);

	///投资者信息
	struct tagTORAInvestorInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Investor[MAX_CODE_LENGTH + 1]; //投资者代码
		char InvestorName[MAX_NAME_LENGTH + 1]; //投资者名称
		//
	};
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, INVESTOR, BROKER);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, INVESTOR, USER);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, INVESTOR, INVESTOR);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, INVESTOR, INVESTOR_NAME);
	//
	DECLARE_MDB_FIELD_TABLE(TORA_API_EXPORT, TORA, INVESTOR, 4);
	DECLARE_MDB_FIELD_NAME_TABLE(TORA_API_EXPORT, TORA, INVESTOR);

	///资金账户信息
	struct tagTORAAccountInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Account[MAX_CODE_LENGTH + 1]; //投资者帐号
		//
		///经纪公司部门代码
		//char	DepartmentID;

		///资金账户代码
		//TTORATstpAccountIDType	AccountID;

		///币种代码
		char	CurrencyID;

		///上日总资金
		double	PreBalance;

		///总资金
		double	Balance;

		///可用资金
		double	UsefulMoney;

		///可取资金
		double	FetchLimit;

		///上日未交收金额(港股通专用字段)
		double	PreUnDeliveredMoney;

		///可用未交收金额(港股通专用字段)
		double	UnDeliveredMoney;

		///当日入金金额
		double	Deposit;

		///当日出金金额
		double	Withdraw;

		///冻结的资金(港股通该字段不包括未交收部分冻结资金)
		double	FrozenCash;

		///冻结未交收金额(港股通专用)
		double	UnDeliveredFrozenCash;

		///冻结的手续费(港股通该字段不包括未交收部分冻结手续费)
		double	FrozenCommission;

		///冻结未交收手续费(港股通专用)
		double	UnDeliveredFrozenCommission;

		///手续费(港股通该字段不包括未交收部分手续费)
		double	Commission;

		///占用未交收手续费(港股通专用)
		double	UnDeliveredCommission;

		///资金账户类型
		char	AccountType;

		///资金账户所属投资者代码
		char	InvestorID[16];

		///银行代码
		char	BankID;

		///银行账户
		char	BankAccountID[31];

		///权利金收入(两融专用)
		double	RoyaltyIn;

		///权利金支出(两融专用)
		double	RoyaltyOut;

		///融券卖出金额(两融专用)
		double	CreditSellAmount;

		///融券卖出使用金额(用于偿还融资负债或买特殊品种的金额)(两融专用)
		double	CreditSellUseAmount;

		///虚拟资产(两融专用)
		double	VirtualAssets;

		///融券卖出金额冻结(用于偿还融资负债或买特殊品种的未成交冻结金额)(两融专用)
		double	CreditSellFrozenAmount;
	};
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, BROKER);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, USER);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, ACCOUNT);
	//
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, CurrencyID);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, PreBalance);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, Balance);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, UsefulMoney);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, FetchLimit);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, PreUnDeliveredMoney);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, UnDeliveredMoney);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, Deposit);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, Withdraw);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, FrozenCash);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, UnDeliveredFrozenCash);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, FrozenCommission);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, UnDeliveredFrozenCommission);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, Commission);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, UnDeliveredCommission);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, AccountType);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, InvestorID);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, BankID);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, BankAccountID);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, RoyaltyIn);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, RoyaltyOut);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, CreditSellAmount);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, CreditSellUseAmount);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, VirtualAssets);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, ACCOUNT, CreditSellFrozenAmount);
	DECLARE_MDB_FIELD_TABLE(TORA_API_EXPORT, TORA, ACCOUNT, 28);
	DECLARE_MDB_FIELD_NAME_TABLE(TORA_API_EXPORT, TORA, ACCOUNT);

	///持仓信息
	struct tagTORAPositionInfo
	{
		char Broker[MAX_CODE_LENGTH + 1]; //经纪公司代码
		char User[MAX_CODE_LENGTH + 1]; //用户代码
		char Investor[MAX_CODE_LENGTH + 1]; //投资者代码
		char Exchange[MAX_EXCHANGE_LENGTH + 1]; //市场
		char Code[MAX_CODE_LENGTH + 1]; //代码
		char Currency; //币种 CURRENCY_CNY
		double Price; //价格
		double Volume;  //量
		double YVolume;  //昨量
		double FrozenVolume; //冻结量
		char Direction; //持仓方向 DIRECTION_LONG
		double Profit; //持仓盈亏
	};
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, BROKER);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, USER);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, INVESTOR);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, EXCHANGE);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, CODE);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, CURRENCY);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, PRICE);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, VOLUME);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, YVOLUME);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, FROZEN_VOLUME);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, DIRECTION);
	DECLARE_MDB_FIELD(TORA_API_EXPORT, TORA, POSITION, PROFIT);
	DECLARE_MDB_FIELD_TABLE(TORA_API_EXPORT, TORA, POSITION, 12);
	DECLARE_MDB_FIELD_NAME_TABLE(TORA_API_EXPORT, TORA, POSITION);

#pragma pack(pop)

#if defined(__cplusplus)
}

#ifdef STR_TORA_MODULE

#include "TORATstpXMdApi.h"
#include "TORATstpTraderApi.h"

	inline const char STATUS_TORA2ZQDB(const char status) {
		switch (status)
		{
		case TORASTOCKAPI::TORA_TSTP_OST_Cached:
			return ORDER_STATUS_NOTTRADED;
			break;
		case TORASTOCKAPI::TORA_TSTP_OST_Unknown:
			return ORDER_STATUS_SUBMITTING;
			break;
		case TORASTOCKAPI::TORA_TSTP_OST_Accepted:
			return ORDER_STATUS_NOTTRADED;
			break;
		case TORASTOCKAPI::TORA_TSTP_OST_PartTraded:
			return ORDER_STATUS_PARTTRADED;
			break;
		case TORASTOCKAPI::TORA_TSTP_OST_AllTraded:
			return ORDER_STATUS_ALLTRADED;
			break;
		case TORASTOCKAPI::TORA_TSTP_OST_PartTradeCanceled:
			return ORDER_STATUS_CANCELLED;
			break;
		case TORASTOCKAPI::TORA_TSTP_OST_AllCanceled:
			return ORDER_STATUS_CANCELLED;
			break;
		case TORASTOCKAPI::TORA_TSTP_OST_Rejected:
			return ORDER_STATUS_REJECTED;
			break;
		case TORASTOCKAPI::TORA_TSTP_OST_SendTradeEngine:
			return ORDER_STATUS_SUBMITTING;
			break;
			break;
		default:
			break;
		}
		return 0;
	}

	inline const char DIRECTION_ZQDB2TORA(const char direction) {
		switch (direction)
		{
		case DIRECTION_LONG:
			return TORASTOCKAPI::TORA_TSTP_D_Buy;
		case DIRECTION_SHORT:
			return TORASTOCKAPI::TORA_TSTP_D_Sell;
		}
		return 0;
	}
	inline const char DIRECTION_TORA2ZQDB(const char direction) {
		switch (direction)
		{
		case TORASTOCKAPI::TORA_TSTP_D_Buy:
			return DIRECTION_LONG;
		case TORASTOCKAPI::TORA_TSTP_D_Sell:
			return DIRECTION_SHORT;
		}
		return 0;
	}

	/*inline const char ORDERTYPE_ZQDB2TORA(const char type, TORASTOCKAPI::TTORATstpExchangeIDType exchange) {
		switch (type)
		{
		case ORDER_LIMIT:
			return TORASTOCKAPI::TORA_TSTP_OPT_LimitPrice;
		case ORDER_MARKET:
			return TORASTOCKAPI::TORA_TSTP_OPT_BestPrice;
		default:
			break;
		}
		return 0;
	}
	inline const char ORDERTYPE_TORA2ZQDB(const char type, const char tc, const char vc) {
		switch (type)
		{
		case TORASTOCKAPI::TORA_TSTP_OPT_LimitPrice: {
			if (tc == TORASTOCKAPI::TORA_TSTP_TC_IOC) {
				if (vc == TORASTOCKAPI::TORA_TSTP_VC_AV) {
					return ORDER_FAK;
				}
				else if (vc == TORASTOCKAPI::TORA_TSTP_VC_CV) {
					return ORDER_FOK;
				}
			}
			return ORDER_LIMIT;
		} break;
		case TORASTOCKAPI::TORA_TSTP_OPT_AnyPrice: {
			return ORDER_MARKET;
		} break;
		default:
			break;
		}
		return ORDER_MARKET;
	}*/

	inline const char OFFSET_TORA2ZQDB(const char direction) {
		switch (direction)
		{
		case TORASTOCKAPI::TORA_TSTP_D_Buy:
			return OFFSET_OPEN;
		case TORASTOCKAPI::TORA_TSTP_D_Sell:
			return OFFSET_CLOSE;
		}
		return 0;
	}

	inline const char OFFSET_ZQDB2TORA(const char offset) {
		switch (offset)
		{
		case OFFSET_OPEN:
			return TORASTOCKAPI::TORA_TSTP_D_Buy;
		case OFFSET_CLOSE:
			return TORASTOCKAPI::TORA_TSTP_D_Sell;
		}
		return 0;
	}

#endif//

#endif//

#endif//_H_ZQTORA_H_