#pragma once

#include "view.h"

namespace zqdb {

	void SetFilterCalcFunc(wxString name);
	wxString GetFilterCalcFunc();

	void SetSortCalcFunc(wxString name);
	wxString GetSortCalcFunc();

	void SetTechKScale(size_t scale);
	size_t GetTechKScale();
	void SetTechKType(CALC_KLINE_TYPE type);
	CALC_KLINE_TYPE GetTechKType();
	void SetTechCycleCur(PERIODTYPE cycle);
	size_t GetTechCycleCur();
	void SetTechCycleAnySec(int sec);
	size_t GetTechCycleAnySec();
	void SetTechCycleAnyMin(int min);
	size_t GetTechCycleAnyMin();

	void SetDefaultTradeByAmount(bool by_amount);
	bool GetDefaultTradeByAmount();
	void SetDefaultTradeValue(double value);
	double GetDefaultTradeValue();
	//void SetTechTradeCloseVolume(double volume);
	//double GetTechTradeCloseVolume();

	zqdb::Calc::InputAttr GetCalcInputAttr(HZQDB h);
	size_t GetCalcTarget(HZQDB h);
	PERIODTYPE GetCalcCycle(HZQDB h);
	size_t GetCalcCycleEx(HZQDB h);
	void SetCalcCycle(HZQDB h, PERIODTYPE cycle, size_t cycleex = 0);

	class TechGeneralPanel : public wxPanel
	{
		typedef wxPanel Base;
	protected:
		size_t anysec_ = 0;
		size_t anymin_ = 0;
		bool trade_by_amount_ = false;
		double trade_value_ = 0;
		//double trade_close_volume_ = 0;
		wxSpinCtrl *ctrl_cycle_anysec_ = nullptr;
		wxSpinCtrl *ctrl_cycle_anymin_ = nullptr;
		wxCheckBox *chk_trade_by_amount_ = nullptr;
		wxStaticText* stc_trade_value_ = nullptr;
		wxSpinCtrlDouble *ctrl_trade_value_ = nullptr;
		//wxStaticText* stc_trade_close_volume_ = nullptr;
		//wxSpinCtrlDouble *ctrl_trade_close_volume_ = nullptr;
	public:
		TechGeneralPanel(wxWindow *parent);

		void Save();

	protected:
		void UpdateTradeUI();
		void OnChkTradeByAmount(wxCommandEvent& WXUNUSED(evt));
		wxDECLARE_EVENT_TABLE();
	};

	class CalcFuncPanel : public wxPanel
	{
		typedef wxPanel Base;
	private:
		zqdb::Calc::Func func_;
		wxPropertyGrid* pg_input_ = nullptr;
		wxRadioBox *radio_target_ = nullptr;
		wxSizer *sizer_target_ = nullptr;
		wxComboBox* cmb_cycle_ = nullptr;
		wxSpinCtrl *ctrl_cycleex_ = nullptr;
		wxStaticBox *box_cycle_ = nullptr;
		wxTextCtrl* ctrl_desc_ = nullptr;
	public:
		CalcFuncPanel(wxWindow *parent);
		virtual ~CalcFuncPanel();

		void SetFunc(zqdb::Calc::Func& func);
		inline zqdb::Calc::Func& GetFunc() { return func_; }

		bool IsChanged();
		void Save();
		void SaveIfChanged();

	protected:
		//
		void OnPropertyGridChange(wxPropertyGridEvent& event);
		//void OnPropertyGridChanging(wxPropertyGridEvent& event);

		void UpdateCycle();
		void OnCmbCycleUpdate(wxCommandEvent& event);
		
		wxDECLARE_EVENT_TABLE();
	};

	class CalcFuncsPanel : public wxPanel
	{
		typedef wxPanel Base;
	private:
		zqdb::Calc::AllFunc all_funcs_;
		wxListBox* list_funcs_ = nullptr;
		CalcFuncPanel* panel_func_ = nullptr;
	public:
		CalcFuncsPanel(wxWindow *parent, CALC_TYPE type, const char* name);
		virtual ~CalcFuncsPanel();

		bool IsChanged();
		void Save();
		void SaveIfChanged();

	protected:
		//
		void UpdateList(const char* name);
		void UpdateSel();
		void OnListbox(wxCommandEvent& event);
		void OnHyperlink(wxHyperlinkEvent& event);

		wxDECLARE_EVENT_TABLE();
	};

	class TechDlg : public wxDialog
	{
		typedef wxDialog Base;
	private:
		enum ID_COMMANDS
		{
			// these should be in the same order as Type_XXX elements above
			ID_BOOK_NOTEBOOK = wxID_HIGHEST,
			ID_BOOK_LISTBOOK,
			ID_BOOK_CHOICEBOOK,
			ID_BOOK_TREEBOOK,
			ID_BOOK_TOOLBOOK,
			ID_BOOK_AUINOTEBOOK,
			ID_BOOK_SIMPLEBOOK,
			ID_BOOK_MAX,

			ID_ORIENT_DEFAULT,
			ID_ORIENT_TOP,
			ID_ORIENT_BOTTOM,
			ID_ORIENT_LEFT,
			ID_ORIENT_RIGHT,
			ID_ORIENT_MAX,
			ID_SHOW_IMAGES,
			ID_FIXEDWIDTH,
			ID_MULTI,
			ID_NOPAGETHEME,
			ID_BUTTONBAR,
			ID_HORZ_LAYOUT,
			ID_ADD_PAGE,
			ID_ADD_PAGE_NO_SELECT,
			ID_INSERT_PAGE,
			ID_DELETE_CUR_PAGE,
			ID_DELETE_LAST_PAGE,
			ID_NEXT_PAGE,
			ID_ADD_PAGE_BEFORE,
			ID_ADD_SUB_PAGE,
			ID_CHANGE_SELECTION,
			ID_SET_SELECTION,
			ID_GET_PAGE_SIZE,
			ID_SET_PAGE_SIZE,

#if wxUSE_HELP
			ID_CONTEXT_HELP,
#endif // wxUSE_HELP
			ID_HITTEST
		};
		// Sample setup
		enum BookType
		{
			Type_Notebook,
			Type_Listbook,
			Type_Choicebook,
			Type_Treebook,
			Type_Toolbook,
			Type_AuiNotebook,
			Type_Simplebook,
			Type_Max
		} m_type;
		int m_orient;
		bool m_chkShowImages;
		bool m_fixedWidth;
		bool m_multi;
		bool m_noPageTheme;
		bool m_buttonBar;
		bool m_horzLayout;
		wxBookCtrlBase *m_bookCtrl;
		TechGeneralPanel* general_panel_ = nullptr;
		wxButton* btn_ok_ = nullptr;
		wxButton* btn_cancel_ = nullptr;
	public:
		
		TechDlg(wxWindow *parent, CALC_TYPE type);
		TechDlg(wxWindow *parent, int type = -1, const char* name = nullptr);
		virtual ~TechDlg();

	protected:

#if wxUSE_NOTEBOOK
		void OnPageChanging(wxNotebookEvent& event);
		void OnPageChanged(wxNotebookEvent& event);
#endif
		void OnOK(wxCommandEvent& event);
		void OnCancel(wxCommandEvent& event);

		wxDECLARE_EVENT_TABLE();
	};

}


