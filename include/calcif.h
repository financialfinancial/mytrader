#pragma once

#ifndef _H_CALC_IF_H_
#define _H_CALC_IF_H_

#include <calc.h>
#include <zqdbif.h>

#if defined(__cplusplus)
extern "C" {
#endif//

#pragma pack(push, 1)

typedef int(*ZQDB_ORDER_SEND_CB)(HZQDB user, HZQDB code, char direction, char offset, char type, double volume, double price, HNMSG* rsp, size_t timeout);
typedef int(*ZQDB_ORDER_CANCEL_CB)(HZQDB user, HZQDB order, HNMSG* rsp, size_t timeout);
typedef int(*ZQDB_ORDER_CLOSE_CB)(HZQDB user, HZQDB position, char type, double volume, double price, HNMSG* rsp, size_t timeout);

//CalcData

#define ZQDB_CALC_DATA_UPDATE_MARKET 0X01 //行情变化
#define ZQDB_CALC_DATA_UPDATE_BAR 0X02 //新增K线
//
typedef void*(*ZQDB_CALC_DATA_Open_CB)(HZQDB code, PERIODTYPE cycle, size_t cycleex, size_t flags);
typedef void(*ZQDB_CALC_DATA_Close_CB)(void* data);
typedef void(*ZQDB_CALC_DATA_Update_CB)(void* data, HZQDB h, ZQDB_NOTIFY_TYPE notify);
typedef size_t(*ZQDB_CALC_DATA_GetUpdateFlags_CB)(void* data);
typedef void(*ZQDB_CALC_DATA_PostUpdate_CB)(void* data);
typedef void*(*ZQDB_CALC_DATA_GetFieldValue_CB)(void* data, MDB_FIELD* field);
typedef size_t(*ZQDB_CALC_DATA_GetDataMaxCount_CB)(void* data);
typedef size_t(*ZQDB_CALC_DATA_GetDataCount_CB)(void* data);
typedef void*(*ZQDB_CALC_DATA_GetDataValue_CB)(void* data, MDB_FIELD* field);

struct ZQDB_CALC_INF
{
	int xmlflag;
	const char* xml;
	ZQDB_MSG_CB msg_cb;
	ZQDB_NOTIFY_CB notify_cb;
	//
	ZQDB_ORDER_SEND_CB order_send_cb;
	ZQDB_ORDER_CANCEL_CB order_cancel_cb; 
	ZQDB_ORDER_CLOSE_CB order_close_cb;
	//
	ZQDB_CALC_DATA_Open_CB CALC_DATA_Open_CB;
	ZQDB_CALC_DATA_Close_CB CALC_DATA_Close_CB;
	ZQDB_CALC_DATA_Update_CB CALC_DATA_Update_CB;
	ZQDB_CALC_DATA_GetUpdateFlags_CB CALC_DATA_GetUpdateFlags_CB;
	ZQDB_CALC_DATA_PostUpdate_CB CALC_DATA_PostUpdate_CB;
	ZQDB_CALC_DATA_GetFieldValue_CB CALC_DATA_GetFieldValue_CB;
	ZQDB_CALC_DATA_GetDataMaxCount_CB CALC_DATA_GetDataMaxCount_CB;
	ZQDB_CALC_DATA_GetDataCount_CB CALC_DATA_GetDataCount_CB;
	ZQDB_CALC_DATA_GetDataValue_CB CALC_DATA_GetDataValue_CB;
};

CALC_API_EXPORT int ZQCalcInit(ZQDB_CALC_INF* inf);
CALC_API_EXPORT void ZQCalcTerm();

CALC_API_EXPORT int ZQCalcStart();
CALC_API_EXPORT void ZQCalcStop(size_t flags);

#pragma pack(pop)

#if defined(__cplusplus)
}

#endif//

#endif//_H_CALC_IF_H_