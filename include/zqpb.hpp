#pragma once

#include "zqdb.h"
#include "zqstr.hpp"
#include "zqdb.pb.h"
#include "mdbdf.hpp"

namespace zqdb {

	template<class TPBCodeInfo, class TCODEINFO>
	inline size_t UpdateCodeInfo(HZQDB h, const TPBCodeInfo& one, TCODEINFO* data, MDB_FIELD* field)
	{
		size_t field_num = 0;
		for (auto val : one.values())
		{
			field[field_num].index = val.id();
			ZQDBNormalizeField(h, field + field_num, 1);
			auto varvalue = val.value();
			switch (field[field_num].type)
			{
			case MDB_FIELD_TYPE_CHAR: {
				*((char*)data + field[field_num].offset) = varvalue.str()[0];
			} break;
			case MDB_FIELD_TYPE_BYTE: {
				*(byte*)((char*)data + field[field_num].offset) = varvalue.i32();
			} break;
			case MDB_FIELD_TYPE_INT16: {
				*(int16_t*)((char*)data + field[field_num].offset) = varvalue.i32();
			} break;
			case MDB_FIELD_TYPE_UINT16: {
				*(uint16_t*)((char*)data + field[field_num].offset) = varvalue.i32();
			} break;
			case MDB_FIELD_TYPE_INT32: {
				*(int32_t*)((char*)data + field[field_num].offset) = varvalue.i32();
			} break;
			case MDB_FIELD_TYPE_UINT32: {
				*(uint32_t*)((char*)data + field[field_num].offset) = varvalue.u32();
			} break;
			case MDB_FIELD_TYPE_INT64: {
				*(int64_t*)((char*)data + field[field_num].offset) = varvalue.i64();
			} break;
			case MDB_FIELD_TYPE_UINT64: {
				*(uint64_t*)((char*)data + field[field_num].offset) = varvalue.u64();
			} break;
			case MDB_FIELD_TYPE_FLOAT: {
				*(float*)((char*)data + field[field_num].offset) = varvalue.f32();
			} break;
			case MDB_FIELD_TYPE_DOUBLE: {
				*(double*)((char*)data + field[field_num].offset) = varvalue.f64();
			} break;
			case MDB_FIELD_TYPE_STRING: {
				strncpy((char*)data + field[field_num].offset, varvalue.str().c_str(), field[field_num].size);
			} break;
			default: {
				memcpy((char*)data + field[field_num].offset, varvalue.data().data(), std::min<>((size_t)field[field_num].size, varvalue.data().size()));
			} break;
			}
			field_num++;
		}
		return field_num;
	}

	template<class TPBCodeInfo, class TCODEINFO>
	inline void UpdateCodeInfoNoID(HZQDB h, const TPBCodeInfo& one, TCODEINFO* data, MDB_FIELD* field, size_t field_num)
	{
		ASSERT(one.values_size() == field_num);
		for (size_t i = 0; i < field_num; i++)
		{
			const auto& val = one.values(i);
			switch (field[i].type)
			{
			case MDB_FIELD_TYPE_CHAR: {
				*((char*)data + field[i].offset) = val.str()[0];
			} break;
			case MDB_FIELD_TYPE_BYTE: {
				*(byte*)((char*)data + field[i].offset) = val.i32();
			} break;
			case MDB_FIELD_TYPE_INT16: {
				*(int16_t*)((char*)data + field[i].offset) = val.i32();
			} break;
			case MDB_FIELD_TYPE_UINT16: {
				*(uint16_t*)((char*)data + field[i].offset) = val.i32();
			} break;
			case MDB_FIELD_TYPE_INT32: {
				*(int32_t*)((char*)data + field[i].offset) = val.i32();
			} break;
			case MDB_FIELD_TYPE_UINT32: {
				*(uint32_t*)((char*)data + field[i].offset) = val.u32();
			} break;
			case MDB_FIELD_TYPE_INT64: {
				*(int64_t*)((char*)data + field[i].offset) = val.i64();
			} break;
			case MDB_FIELD_TYPE_UINT64: {
				*(uint64_t*)((char*)data + field[i].offset) = val.u64();
			} break;
			case MDB_FIELD_TYPE_FLOAT: {
				*(float*)((char*)data + field[i].offset) = val.f32();
			} break;
			case MDB_FIELD_TYPE_DOUBLE: {
				*(double*)((char*)data + field[i].offset) = val.f64();
			} break;
			case MDB_FIELD_TYPE_STRING: {
				strncpy((char*)data + field[i].offset, val.str().c_str(), field[i].size);
			} break;
			default: {
				memcpy((char*)data + field[i].offset, val.data().data(), std::min<size_t>(field[i].size, val.data().size()));
			} break;
			}
		}
	}

	template<class TPBCodeInfo, class TCODEINFO>
	inline void BuildCodeInfo(TPBCodeInfo* one, const TCODEINFO* data, MDB_FIELD* field, size_t field_num)
	{
		one->set_exchange(data->Exchange);
		one->set_code(data->Code);
		for (size_t i = 0; i < field_num; i++)
		{
			auto val = one->add_values();
			val->set_id(field[i].index);
			auto varval = val->mutable_value();
			switch (field[i].type)
			{
			case MDB_FIELD_TYPE_CHAR: {
				varval->set_str((const char*)data + field[i].offset, field[i].size);
			} break;
			case MDB_FIELD_TYPE_BYTE: {
				varval->set_i32(*(uint8_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_INT16: {
				varval->set_i32(*(int16_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_UINT16: {
				varval->set_i32(*(uint16_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_INT32: {
				varval->set_i32(*(int32_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_UINT32: {
				varval->set_u32(*(uint32_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_INT64: {
				varval->set_i64(*(int64_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_UINT64: {
				varval->set_u64(*(uint64_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_FLOAT: {
				varval->set_f32(*(float*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_DOUBLE: {
				varval->set_f64(*(double*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_STRING: {
				varval->set_str((const char*)data + field[i].offset, field[i].size);
			} break;
			default: {
				varval->set_data((const char*)data + field[i].offset, field[i].size);
			} break;
			}
		}
	}

	template<class TPBCodeInfo, class TCODEINFO>
	inline void BuildCodeInfoNoID(TPBCodeInfo* one, const TCODEINFO* data, MDB_FIELD* field, size_t field_num)
	{
		one->set_exchange(data->Exchange);
		one->set_code(data->Code);
		for (size_t i = 0; i < field_num; i++)
		{
			auto val = one->add_values();
			switch (field[i].type)
			{
			case MDB_FIELD_TYPE_CHAR: {
				val->set_str((const char*)data + field[i].offset, field[i].size);
			} break;
			case MDB_FIELD_TYPE_BYTE: {
				val->set_i32(*(uint8_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_INT16: {
				val->set_i32(*(int16_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_UINT16: {
				val->set_i32(*(uint16_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_INT32: {
				val->set_i32(*(int32_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_UINT32: {
				val->set_u32(*(uint32_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_INT64: {
				val->set_i64(*(int64_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_UINT64: {
				val->set_u64(*(uint64_t*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_FLOAT: {
				val->set_f32(*(float*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_DOUBLE: {
				val->set_f64(*(double*)((const char*)data + field[i].offset));
			} break;
			case MDB_FIELD_TYPE_STRING: {
				val->set_str((const char*)data + field[i].offset, field[i].size);
			} break;
			default: {
				val->set_data((const char*)data + field[i].offset, field[i].size);
			} break;
			}
		}
	}

	template<class TIDS>
	inline void BuildField(HZQDB h, MDB_FIELD* field, const TIDS& ids, size_t count) {
		for (size_t i = 0; i < count; i++)
		{
			field[i].index = ids[i];
		}
		ZQDBNormalizeField(h, field, count);
	}

	inline size_t BuildFullDiffField(MDB_FIELD* diff_field) {
		size_t diff_field_num = 0;
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, DATE);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, TIME);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, YCLOSE);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, YSETTLE);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, OPEN);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, HIGH);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, LOW);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, CLOSE);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, SETTLE);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, AMOUNT);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, VOLUME);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BID1);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BID2);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BID3);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BID4);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BID5);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BIDV1);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BIDV2);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BIDV3);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BIDV4);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, BIDV5);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASK1);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASK2);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASK3);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASK4);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASK5);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASKV1);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASKV2);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASKV3);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASKV4);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, CODE, ASKV5);
		return diff_field_num;
	}

	inline size_t BuildFullFTDiffField(MDB_FIELD* diff_field) {
		size_t diff_field_num = 0;
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, DATE);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, TIME);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, YCLOSE);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, YSETTLE);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, OPEN);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, HIGH);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, LOW);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, CLOSE);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, SETTLE);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, AMOUNT);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, VOLUME);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BID1);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BID2);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BID3);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BID4);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BID5);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BIDV1);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BIDV2);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BIDV3);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BIDV4);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, BIDV5);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASK1);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASK2);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASK3);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASK4);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASK5);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASKV1);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASKV2);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASKV3);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASKV4);
		//diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, ASKV5);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, PreOpenInterest);
		diff_field[diff_field_num++] = MDB_FIELD_TABLE_FIELD(ZQDB, FTCODE, OpenInterest);
		return diff_field_num;
	}

	template<class TSubs, class TCODEINFO = CODEINFO>
	inline bool BuildFullRtnSubscribeData(com::zqdb::proto::msg::RtnSubscribe& rtn, const TSubs& subs) {
		MDB_FIELD diff_field[256] = { 0 };
		size_t diff_field_num = BuildFullDiffField(diff_field);
		if (diff_field_num > 0) {
			for (auto h : subs) {
				BuildCodeInfo(rtn.add_codes(), (TCODEINFO*)ZQDBGetValue(h), diff_field, diff_field_num);
			}
			return true;
		}
		return false;
	}

	template<class TCODEINFO = CODEINFO>
	inline bool BuildFullRtnSubscribeData(com::zqdb::proto::msg::RtnSubscribe& rtn, TCODEINFO* newdata) {
		MDB_FIELD diff_field[256] = { 0 };
		size_t diff_field_num = BuildFullDiffField(diff_field);
		if (diff_field_num > 0) {
			BuildCodeInfo(rtn.add_codes(), newdata, diff_field, diff_field_num);
			return true;
		}
		return false;
	}

	template<class TSubs, class TCODEINFO = FTCODEINFO>
	inline bool BuildFullFTRtnSubscribeData(com::zqdb::proto::msg::RtnSubscribe& rtn, const TSubs& subs) {
		MDB_FIELD diff_field[256] = { 0 };
		size_t diff_field_num = BuildFullFTDiffField(diff_field);
		if (diff_field_num > 0) {
			for (auto h : subs) {
				BuildRtnSubscribeCodeInfo(rtn.add_codes(), (TCODEINFO*)ZQDBGetValue(h), diff_field, diff_field_num);
			}
			return true;
		}
		return false;
	}

	template<class TCODEINFO = FTCODEINFO>
	inline bool BuildFullFTRtnSubscribeData(com::zqdb::proto::msg::RtnSubscribe& rtn, TCODEINFO* newdata) {
		MDB_FIELD diff_field[256] = { 0 };
		size_t diff_field_num = BuildFullFTDiffField(diff_field);
		if (diff_field_num > 0) {
			BuildCodeInfo(rtn.add_codes(), newdata, diff_field, diff_field_num);
			return true;
		}
		return false;
	}

	template<class TCODEINFO = CODEINFO>
	inline bool BuildDiffRtnSubscribeData(com::zqdb::proto::msg::RtnSubscribe& rtn
		, const MDB_FIELD* field, size_t field_num, TCODEINFO* newdata, TCODEINFO* olddata) {
		MDB_FIELD diff_field[256] = { 0 };
		size_t diff_field_num = mdb::FieldOp::Diff(field, field_num, (const char*)newdata, (const char*)olddata, diff_field);
		if (diff_field_num > 0) {
			BuildCodeInfo(rtn.add_codes(), newdata, diff_field, diff_field_num);
			return true;
		}
		return false;
	}
}
