#pragma once

#define HOME_URL "https://www.mytrader.org.cn/"

#define LOCAL_CONFIG_DIR "mytrader"
#define LOCAL_DESC_FILE "desc.json"
#define LOCAL_SETTINGS_FILE "settings.json"

#define LOCAL_MDB_DIR "mdb"
#define LOCAL_ZQDB_DIR "zqdb"

#define LOCAL_UPDATE_DIR "update"
#define LOCAL_UPDATE_DESC_FILE "mytrader.json"
#define LOCAL_UPDATE_APP_FILE "mytrader.zip"
