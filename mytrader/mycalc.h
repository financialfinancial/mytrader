#pragma once

#include <calcif.h>
#include <XUtil\XXml.hpp>

extern "C" {
	void* ZQDB_CALC_DATA_Open(HZQDB code, PERIODTYPE cycle, size_t cycleex, size_t flags);
	void ZQDB_CALC_DATA_Close(void* data);
	void ZQDB_CALC_DATA_Update(void* data, HZQDB h, ZQDB_NOTIFY_TYPE notify);
	size_t ZQDB_CALC_DATA_GetUpdateFlags(void* data);
	void ZQDB_CALC_DATA_PostUpdate(void* data);
	void* ZQDB_CALC_DATA_GetFieldValue(void* data, MDB_FIELD* field);
	size_t ZQDB_CALC_DATA_GetDataMaxCount(void* data);
	size_t ZQDB_CALC_DATA_GetDataCount(void* data);
	void* ZQDB_CALC_DATA_GetDataValue(void* data, MDB_FIELD* field);
}

namespace zqdb {

	class CalcData : public HandleEx, public CALCDATA
	{
		typedef HandleEx Handle;
	protected:
		size_t count_ = 0;
		size_t offset_ = 0;
		std::vector<char> kdata_;
		std::map<MDB_FIELD, std::vector<char>, mdb::FIELDLess> data_;
		std::array<std::vector<double>, 4> prices_; //�ر�۸�
		size_t update_flag_ = 0;
	public:
		CalcData(HZQDB code, PERIODTYPE cycle, size_t cycleex, size_t flags = 0);
		~CalcData();

		void Update(HZQDB h, ZQDB_NOTIFY_TYPE notify);
		void UpdateEx();
		void UpdateEx(int fieldex, bool force = false);
		size_t GetUpdateFlags();
		void PostUpdate();

		void* GetFieldValue(MDB_FIELD& field);

		size_t GetDataMaxCount();
		size_t GetDataCount();
		void* GetDataValue(MDB_FIELD& field);
	};
}
