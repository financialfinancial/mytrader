#include "mycalc.h"

extern "C" {

	void* ZQDB_CALC_DATA_Open(HZQDB code, PERIODTYPE cycle, size_t cycleex, size_t flags)
	{
		return new zqdb::CalcData(code, cycle, cycleex, flags);
	}

	void ZQDB_CALC_DATA_Close(void* data)
	{
		delete (zqdb::CalcData*)data;
	}

	void ZQDB_CALC_DATA_Update(void* data, HZQDB h, ZQDB_NOTIFY_TYPE notify)
	{
		((zqdb::CalcData*)data)->Update(h, notify);
	}

	size_t ZQDB_CALC_DATA_GetUpdateFlags(void* data)
	{
		return ((zqdb::CalcData*)data)->GetUpdateFlags();
	}

	void ZQDB_CALC_DATA_PostUpdate(void* data)
	{
		((zqdb::CalcData*)data)->PostUpdate();
	}

	void* ZQDB_CALC_DATA_GetFieldValue(void* data, MDB_FIELD* field)
	{
		return ((zqdb::CalcData*)data)->GetFieldValue(*field);
	}

	size_t ZQDB_CALC_DATA_GetDataMaxCount(void* data)
	{
		return ((zqdb::CalcData*)data)->GetDataMaxCount();
	}

	size_t ZQDB_CALC_DATA_GetDataCount(void* data)
	{
		return ((zqdb::CalcData*)data)->GetDataCount();
	}

	void* ZQDB_CALC_DATA_GetDataValue(void* data, MDB_FIELD* field)
	{
		return ((zqdb::CalcData*)data)->GetDataValue(*field);
	}

}

namespace zqdb {

	CalcData::CalcData(HZQDB code, PERIODTYPE cycle, size_t cycleex, size_t flags)
		: Handle(ZQDB_HANDLE_TYPE_UNKNOWN, flags)
		, CALCDATA{ code, cycle, cycleex }
	{
		if (!ZQDBIsSubscribeMarketDataAll(code)) {
			if (HasFlags(CALC_DATA_FLAG_SYNC_DATA)) {
				ZQDBSyncData(code, cycle, cycleex, false);
			}
			if (HasFlags(CALC_DATA_FLAG_ASYNC_DATA)) {
				ZQDBSyncData(code, cycle, cycleex, true);
			}
		}
		if (HasFlags(CALC_DATA_FLAG_SUB_DATA)) {
			ZQDBCalcSubscribe(code);
		}
	}

	CalcData::~CalcData()
	{
		if (HasFlags(CALC_DATA_FLAG_SUB_DATA)) {
			ZQDBCalcUnSubscribe(h);
		}
	}

	void CalcData::Update(HZQDB h, ZQDB_NOTIFY_TYPE notify)
	{
		if (h != this->h) {
			return;
		}

		update_flag_ |= ZQDB_CALC_DATA_UPDATE_MARKET;

		auto old_count = count_;
		auto old_offset = offset_;

		HZQDB code = this->h;
		DataGuard guard(code, cycle, cycleex);
		size_t max_count = 0;
		size_t count = 0, elem_sz = 0;
		max_count = ZQDBGetDataMaxCount(code, cycle, cycleex);
		count = ZQDBGetDataCount(code, cycle, cycleex, &elem_sz);
		if (max_count < count) {
			max_count = count;
		}
		count_ = count;
		offset_ = count_;

		if (count_ <= 0 || elem_sz <= 0) {
			return;
		}
		if (data_.empty() || kdata_.empty()) {
			std::vector<MDB_FIELD> attr;
			attr.resize(ZQDBGetDataAttrCount(code, cycle, cycleex));
			ZQDBGetDataAttr(code, cycle, cycleex, attr.size(), attr.data());
			ASSERT(elem_sz == MDBFieldCalcSize(attr.data(), attr.size()));
			kdata_.resize(elem_sz);
			for (size_t i = 0; i < attr.size(); ++i)
			{
				auto& data = data_[attr[i]];
			}
		}
		ASSERT(!kdata_.empty() && !data_.empty());
		auto kdata = (KDATA*)kdata_.data();
		for (auto& pr : data_)
		{
			pr.second.resize(max_count * pr.first.size);
		}
		//最新数据更有价值，历史数据错了就丢弃
		bool last_invalid = false;
		size_t i = count_;
		for (; i > 0; --i)
		{
			size_t pos = i - 1;
			size_t num = 1;
			if (MDB_STATUS_OK != ZQDBGetDataValue(code, cycle, cycleex, pos, &num, kdata)) {
				break;
			}
			if (kdata->Date == 0
				|| IsInvalidValue(kdata->Open)
				|| IsInvalidValue(kdata->High)
				|| IsInvalidValue(kdata->Low)
				|| IsInvalidValue(kdata->Close)) {
				if (pos == (count_ - 1)) {
					last_invalid = true;
					continue;
				}
				else {
					break;
				}
			}
			offset_ = pos;
			for (auto& pr : data_)
			{
				memcpy(&pr.second[pos * pr.first.size], (char*)kdata + pr.first.offset, pr.first.size);
			}
		}
		ASSERT(count_ >= offset_);
		if (last_invalid) {
			count_ -= 1;
			if (count_ > offset_) {
				count_ -= offset_;
			}
			else {
				count_ = 0;
			}
		}
		else {
			count_ -= offset_;
		}

		//更新特殊字段
		UpdateEx();

		if (old_count != count_) {
			update_flag_ |= ZQDB_CALC_DATA_UPDATE_BAR;
		}
	}

	void CalcData::UpdateEx()
	{
		for (auto fieldex = MAX_MDB_FIELD_INDEX; fieldex > MAX_MDB_FIELD_INDEX - 4; fieldex--)
		{
			UpdateEx(fieldex);
		}
	}

	void CalcData::UpdateEx(int fieldex, bool force)
	{
		switch (fieldex)
		{
		case PRICE_MEDIAN:
		case PRICE_TYPICAL:
		case PRICE_WEIGHTED:
		case PRICE_AVPRICE: {
			auto index = MAX_MDB_FIELD_INDEX - fieldex;
			if (force || !prices_[index].empty()) {
				size_t offset = 0;
				if (prices_[index].empty()) {
					prices_[index].reserve(GetDataMaxCount());
				}
				else {
					offset = prices_[index].size() - 1; //始终更新最后一根数据
				}
				prices_[index].resize(GetDataCount());
				auto buffer = prices_[index].data();
				auto count = prices_[index].size();
				switch (fieldex)
				{
				case PRICE_MEDIAN: {
					auto high = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, HIGH));
					if (high) {
						auto low = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, LOW));
						if (low) {
							for (size_t i = offset; i < count; i++)
							{
								buffer[i] = (high[i] + low[i]) / 2;
							}
						}
					}
				} break;
				case PRICE_TYPICAL: {
					auto high = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, HIGH));
					if (high) {
						auto low = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, LOW));
						if (low) {
							auto close = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, CLOSE));
							if (close) {
								for (size_t i = offset; i < count; i++)
								{
									buffer[i] = (high[i] + low[i] + close[i]) / 3;
								}
							}
						}
					}
				} break;
				case PRICE_WEIGHTED: {
					auto high = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, HIGH));
					if (high) {
						auto low = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, LOW));
						if (low) {
							auto close = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, CLOSE));
							if (close) {
								auto open = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, OPEN));
								if (open) {
									for (size_t i = offset; i < count; i++)
									{
										buffer[i] = (high[i] + low[i] + close[i] + open[i]) / 4;
									}
								}
							}
						}
					}
				} break;
				case PRICE_AVPRICE: {
					auto amount = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, AMOUNT));
					if (amount) {
						auto volume = (double*)GetDataValue(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, VOLUME));
						if (volume) {
							for (size_t i = offset; i < count; i++)
							{
								buffer[i] = amount[i] / volume[i];
							}
						}
					}
				} break;
				}
			}
		} break;
		}
	}

	size_t CalcData::GetUpdateFlags()
	{
		return update_flag_;
	}

	void CalcData::PostUpdate()
	{
		if (update_flag_)
			update_flag_ = 0;
	}

	void* CalcData::GetFieldValue(MDB_FIELD& field)
	{
		if (MDB_STATUS_OK == ZQDBNormalizeField(h, &field, 1)) {
			return ZQDBGetValue(h);
		}
		return nullptr;
	}

	size_t CalcData::GetDataMaxCount()
	{
		if (data_.empty()) {
			return 0;
		}
		return data_.begin()->second.size() / data_.begin()->first.size;
	}

	size_t CalcData::GetDataCount()
	{
		if (data_.empty()) {
			return 0;
		}
		return count_;
	}

	void* CalcData::GetDataValue(MDB_FIELD& field)
	{
		switch (field.index)
		{
		case PRICE_MEDIAN:
		case PRICE_TYPICAL:
		case PRICE_WEIGHTED:
		case PRICE_AVPRICE: {
			auto index = MAX_MDB_FIELD_INDEX - field.index;
			if (prices_[index].size() != GetDataCount()) {
				UpdateEx(field.index, true);
			}
			ASSERT(prices_[index].size() == GetDataCount());
			if (!MDBFieldIsNormalized(field)) {
				field.type = MDB_FIELD_TYPE_DOUBLE;
				field.flags = 0;
				field.size = sizeof(double);
				field.offset = 0;
			}
			return prices_[index].data(); //这里的数据没有offset_无效数据
		} break;
		default: {
			auto it = data_.find(field);
			if (it != data_.end()) {
				if (!MDBFieldIsNormalized(field)) {
					field = it->first;
					field.offset = 0;
				}
				ASSERT(field.size == it->first.size);
				return it->second.data() + it->first.size * offset_;
			}
		} break;
		}
		return nullptr;
	}

}
