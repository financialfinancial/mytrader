///////////////////////////////////////////////////////////////////////////////
// Name:        ribbondemo.cpp
// Purpose:     wxRibbon: Ribbon user interface - sample/test program
// Author:      Peter Cawley
// Modified by:
// Created:     2009-05-25
// Copyright:   (C) Copyright 2009, Peter Cawley
// Licence:     wxWindows Library Licence
///////////////////////////////////////////////////////////////////////////////

#include "myapp.h" 
#include "mylog.h"
#include "mysettingsdlg.h"
#include "mycalc.h"
#include "myctp.h"
#include "mytora.h"
#include "mylogindlg.h"
#include "mytaskbar.h"
#include "mysmartkbdlg.h"
#include "myframe.h"
#include "mycalcframe.h"
#include "mystrategyframe.h"
#include "mydataframe.h"
#include "mywebframe.h"
#include <zqdb.pb.h>

#if wxUSE_CHOICEDLG
#include "wx/choicdlg.h"
#endif // wxUSE_CHOICEDLG

class Global
{
public:
	Global()
	{
		boost::filesystem::path::imbue(
			std::locale(std::locale(), new std::codecvt_utf8_utf16<wchar_t>()));
	}

}g_global;

// this is a definition so can't be in a header
wxDEFINE_EVENT(MY_EVENT, wxCommandEvent);
wxDEFINE_EVENT(ZQDB_NOTIFY_LOG_UPDATE_EVENT, wxStringCommandEvent);

static wxString FormatTips(const char* xml, size_t xmlflag)
{
	CFG_FROM_XML(cfg, xml, xmlflag);
	//std::string s = u8"连接中...";
	std::string utf_content = cfg.get<std::string>("content", "");
	//std::wstring ws = zqdb::utf8_to_unicode(s);
	wxString content = utf2wxString(utf_content.c_str());
	/*switch (cfg.get<int>("type", 0))
	{
	case ZQDB_LOG_TIP_TYPE_MODULE: {
		return utf2wxString(cfg.get<std::string>("module", "").c_str()) + wxT(" ") + content;
	} break;
	case ZQDB_LOG_TIP_TYPE_USER: {
		return utf2wxString(cfg.get<std::string>("module", "").c_str()) + wxT(" ") + utf2wxString(cfg.get<std::string>("user", "").c_str()) + wxT(" ") + content;
	} break;
	case ZQDB_LOG_TIP_TYPE_CALC: {
		return utf2wxString(cfg.get<std::string>("calc", "").c_str()) + wxT(" ") + content;
	} break;
	default: {
	} break;
	}*/
	return content;
}

// -- implementations --

MyEventFilter::MyEventFilter()
{
	wxEvtHandler::AddFilter((MyEventFilter*)this);
}

MyEventFilter::~MyEventFilter()
{
	wxEvtHandler::RemoveFilter(this);
}

int MyEventFilter::FilterEvent(wxEvent& event)
{
	if (MyLoginDlg::Inst()) {
		return Event_Skip;
	}
	const wxEventType t = event.GetEventType(); 
	auto obj = event.GetEventObject();
	if (obj) {
		if (obj->IsKindOf(wxCLASSINFO(wxWindow))) {
			auto wnd = wxDynamicCast(obj, wxWindow);
			if (wnd) {
				auto topWnd = wnd;
				while (wnd = wnd->GetParent())
				{
					topWnd = wnd;
				}
				ASSERT(topWnd);
				if (topWnd->IsKindOf(wxCLASSINFO(wxFrame))) {
					auto frame = wxDynamicCast(topWnd, wxFrame);
					if (wxGetApp().FindTechFrame(frame)) {
						int ret = ((MyTechFrame*)frame)->FilterEvent(event);
						if (ret != Event_Skip) {
							return ret;
						}
					}
				}
				else if (topWnd->IsKindOf(wxCLASSINFO(wxDialog))) {
					auto dlg = wxDynamicCast(topWnd, wxDialog);
					if (dlg == wxGetApp().GetSmartKBDlg()) {
						int ret = ((MySmartKBDlg*)dlg)->FilterEvent(event);
						if (ret != Event_Skip) {
							return ret;
						}
					}
				}
			}
		}
	}
	// Continue processing the event normally as well.
	return Event_Skip;
}

wxIMPLEMENT_APP(MyApp);

#include "transparent.xpm"

MyApp::MyApp() : timer_(this)
{
	// call this to tell the library to call our OnFatalException()
	wxHandleFatalExceptions();

	ZQDBLogSet([](int level, char const* const format, ...)
	{
		if (wxGetApp().IsExitFlag()) {
			return;
		}
		va_list arg_list;
		va_start(arg_list, format);
		//vprintf(format, arg_list);
		char logstr[4096] = { 0 };
		vsnprintf(logstr, 4095, format, arg_list);
		va_end(arg_list);
		ASSERT(strlen(logstr));
		try {
			int xmlflag = -1;
			if (logstr[0] == '{') { //json
				boost::property_tree::ptree cfg;
				if (XUtil::json_from_str(logstr, cfg)) {
					xmlflag = XUtil::XML_FLAG_JSON_STRING;
					wxGetApp().Post([level, cfg = std::move(cfg)]()mutable {
						wxGetApp().ShowTips(level, (const char*)&cfg, XUtil::XML_FLAG_PTREE);
					});
				}
			}
			/*else if (logstr[0] == '<') { //xml
				if (XUtil::xml_from_str(str, cfg)) {
					xmlflag = XUtil::XML_FLAG_XML_STRING;
				}
			}*/
			wxString wxstr = utf2wxString(logstr);
			switch (level)
			{
			case 0: {
				//
			} break;
			case 1: {
				wxLogError(wxT("%s"), wxstr);
			} break;
			case 2: {
				wxLogWarning(wxT("%s"), wxstr);
			} break;
			case 3: {
				wxLogInfo(wxT("%s"), wxstr);
			} break;
			case 4: {
				wxLogDebug(wxT("%s"), wxstr);
			} break;
			default:
				break;
			}
			if (xmlflag < 0) {
				wxGetApp().Post([level, wxstr = std::move(wxstr)]()mutable {
					wxGetApp().ShowTips(level, std::move(wxstr));
				});
			}
		}
		catch (const std::exception& ex) {
			wxLogWarning(wxT("process tips[%s] exception:%s"), utf2wxString(logstr).wx_str(), ex.what());
		}
		catch (...) {
			wxLogWarning(wxT("process tips[%s] exception: unknown"), utf2wxString(logstr));
		}
	});
}

#if !defined(mytraderpro) && !defined(mytraderd)
void MyApp::LoadSkinInfo()
{
	wxScreenDC  dc;

	auto skin_info_ptr = std::make_shared<SkinInfo>();

	auto artProvider = new wxRibbonDefaultArtProvider();
	skin_info_ptr->artProvider = artProvider;
	if (skin_info_ptr->artProvider) {
		skin_info_ptr->artProvider->GetColourScheme(&skin_info_ptr->crPrimary
			, &skin_info_ptr->crSecondary, &skin_info_ptr->crTertiary);
		skin_info_ptr->crViewBkgnd = skin_info_ptr->artProvider->GetColour(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR);		//视图色
		//skin_info_ptr->crCtrlBkgnd = skin_info_ptr->artProvider->GetColour(wxRIBBON_ART_TAB_CTRL_BACKGROUND_COLOUR);		//控件色
		skin_info_ptr->crCtrlForgnd.Set(234, 242, 251);
		skin_info_ptr->crViewForgnd = skin_info_ptr->crTertiary;		//视图文字
		skin_info_ptr->crCtrlForgnd = skin_info_ptr->crTertiary;		//视图文字
	}
	else {
		skin_info_ptr->crPrimary.Set(194, 216, 241);
		skin_info_ptr->crSecondary.Set(255, 223, 114);
		skin_info_ptr->crTertiary.Set(0, 0, 0);
		skin_info_ptr->crViewBkgnd.Set(201, 217, 237);
		skin_info_ptr->crCtrlForgnd.Set(234, 242, 251);
		skin_info_ptr->crViewForgnd = skin_info_ptr->crTertiary;		//视图文字
		skin_info_ptr->crCtrlForgnd = skin_info_ptr->crTertiary;		//视图文字
	}

	skin_info_ptr->curDragLeftRight = wxCursor(wxCURSOR_SIZEWE);
	skin_info_ptr->curDragUpDown = wxCursor(wxCURSOR_SIZENS);
	skin_info_ptr->curDrawLine = wxCursor(wxCURSOR_PENCIL);
	
	skin_info_ptr->crBackgnd.Set(7, 7, 7);		//背景
	skin_info_ptr->crTabSelBackgnd.Set(64, 0, 0);	//标签选中背景
	skin_info_ptr->crRptTitleBakcgnd.Set(27, 27, 27);//报表标题背景
	skin_info_ptr->crRptSelBackgnd.Set(38, 38, 38);	//报表选中背景

	skin_info_ptr->crTitle.Set(128, 128, 128);			//标题
	skin_info_ptr->crText.Set(224, 224, 224);			//文字
	skin_info_ptr->crRising.Set(255, 90, 90);			//上涨
	skin_info_ptr->crFalling.Set(59, 235, 104);		//下跌
	skin_info_ptr->crCommodityCode.Set(80, 248, 255);	//代码
	skin_info_ptr->crCommodityName.Set(252, 252, 84);	//名称
	skin_info_ptr->crAmount.Set(252, 252, 84);			//价
	skin_info_ptr->crVolume.Set(252, 252, 84);			//量
	skin_info_ptr->crTabSel.Set(84, 252, 252);			//标签选中

	skin_info_ptr->crLine.Set(255, 255, 255);			//线
	skin_info_ptr->crAverageLine.Set(252, 252, 84);	//均线
	skin_info_ptr->crDrawLine.Set(255, 0, 255);		//画线
	skin_info_ptr->crXYLine.Set(60, 60, 60);			//X、Y分隔线
	skin_info_ptr->crXText.Set(125, 125, 125);			//X坐标文字
	skin_info_ptr->crYText.Set(125, 125, 125);//QCOLOR_DEF_YAXIS_VALUE;			//Y坐标文字
	skin_info_ptr->crCrossCursor.Set(224, 224, 224);	//十字游标	
	skin_info_ptr->crRptLine.Set(81, 81, 81);		//报表线
	skin_info_ptr->crRisingLine.Set(255, 52, 52);		//上涨线
	skin_info_ptr->crFallingLine.Set(84, 252, 252);	//下跌线
	skin_info_ptr->crRefline.Set(60, 60, 60);
	skin_info_ptr->crILine[0].Set(252, 252, 252);
	skin_info_ptr->crILine[1].Set(252, 252, 84);
	skin_info_ptr->crILine[2].Set(252, 84, 252);
	skin_info_ptr->crILine[3].Set(84, 232, 61);
	skin_info_ptr->crILine[4].Set(255, 109, 0);
	skin_info_ptr->crILine[5].Set(185, 120, 234);
	skin_info_ptr->crILine[6].Set(107, 142, 212);
	skin_info_ptr->crILine[7].Set(164, 164, 137);

	skin_info_ptr->penTitle = wxPen(skin_info_ptr->crTitle, 1);			//和Title颜色一致的线
	skin_info_ptr->penLine = wxPen(skin_info_ptr->crLine, 1);			//线
	skin_info_ptr->penRisingLine = wxPen(skin_info_ptr->crRisingLine, 1);	//上涨线
	skin_info_ptr->penFallingLine = wxPen(skin_info_ptr->crFallingLine, 1);	//下跌线
	skin_info_ptr->penAverageLine = wxPen(skin_info_ptr->crAverageLine, 1);	//均线
	skin_info_ptr->penDrawLine = wxPen(skin_info_ptr->crDrawLine, 1);		//画线
	skin_info_ptr->penXYLine = wxPen(skin_info_ptr->crXYLine, 1);		//X、Y分隔线
	skin_info_ptr->penCrossCursor = skin_info_ptr->penXYLine;	//十字游标	
	skin_info_ptr->penYCrossCursor = skin_info_ptr->penXYLine;																	//skin_info_ptr->penCrossCursor = wxPen(wxColour(80, 80, 80), 0);
	//skin_info_ptr->penCrossCursor = wxPen(wxColour(80, 80, 80), 0);
	//skin_info_ptr->penYCrossCursor = wxPen(wxColour(60, 60, 60), 0);
	//skin_info_ptr->penRptLine = wxPen(skin_info_ptr->crRptLine, 1);		//报表线
	//skin_info_ptr->penILine[0] = wxPen(skin_info_ptr->crILine[0], 1);		//指标线
	//skin_info_ptr->penILine[1] = wxPen(skin_info_ptr->crILine[1], 1);		//指标线
	//skin_info_ptr->penILine[2] = wxPen(skin_info_ptr->crILine[2], 1);		//指标线
	//skin_info_ptr->penILine[3] = wxPen(skin_info_ptr->crILine[3], 1);		//指标线
	//skin_info_ptr->penILine[4] = wxPen(skin_info_ptr->crILine[4], 1);		//指标线
	//skin_info_ptr->penILine[5] = wxPen(skin_info_ptr->crILine[5], 1);		//指标线
	//skin_info_ptr->penILine[6] = wxPen(skin_info_ptr->crILine[6], 1);		//指标线
	//skin_info_ptr->penILine[7] = wxPen(skin_info_ptr->crILine[7], 1);		//指标线
	//skin_info_ptr->penRefline = wxPen(skin_info_ptr->crRefline, 0, wxPENSTYLE_DOT);		//参考线

	skin_info_ptr->brush = wxBrush(skin_info_ptr->crLine);
	skin_info_ptr->brushRising = wxBrush(skin_info_ptr->crRisingLine);
	skin_info_ptr->brushFalling = wxBrush(skin_info_ptr->crFallingLine);
	skin_info_ptr->brushDrawLine = wxBrush(skin_info_ptr->crDrawLine);
	skin_info_ptr->brushCrossCursor = wxBrush(wxColour(38, 38, 38));
	skin_info_ptr->brushNull = *wxTRANSPARENT_BRUSH;

	//skin_info_ptr->bmpNull = wxBitmap(transparent_xpm);

	wxFont font = wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT);
	skin_info_ptr->fontTitle = wxFont(10, font.GetFamily(), font.GetStyle(), font.GetWeight(), font.GetUnderlined()
		, font.GetFaceName());
	skin_info_ptr->fontText = wxFont(9, font.GetFamily(), font.GetStyle(), wxFONTWEIGHT_BOLD, font.GetUnderlined()
		, font.GetFaceName());
	skin_info_ptr->fontRptTitle = wxFont(12, font.GetFamily(), font.GetStyle(), font.GetWeight(), font.GetUnderlined()
		, wxT("宋体"));
	skin_info_ptr->fontRptText = wxFont(12, font.GetFamily(), font.GetStyle(), font.GetWeight(), font.GetUnderlined()
		, wxT("宋体"));
	skin_info_ptr->fontXText = wxFont(12, font.GetFamily(), font.GetStyle(), font.GetWeight(), font.GetUnderlined()
		, wxT("宋体"));
	skin_info_ptr->fontYText = wxFont(12, font.GetFamily(), font.GetStyle(), font.GetWeight(), font.GetUnderlined()
		, wxT("宋体"));

	wxString strText = wxT("高速行情交易系统");
	dc.SetFont(skin_info_ptr->fontTitle);
	skin_info_ptr->xyTitle = dc.GetTextExtent(strText);
	skin_info_ptr->xyTitle.x /= strText.Length();
	dc.SetFont(skin_info_ptr->fontText);
	skin_info_ptr->xyText = dc.GetTextExtent(strText);
	skin_info_ptr->xyText.x /= strText.Length();
	dc.SetFont(skin_info_ptr->fontRptTitle);
	skin_info_ptr->xyRptTitle = dc.GetTextExtent(strText);
	skin_info_ptr->xyRptTitle.x /= strText.Length();
	dc.SetFont(skin_info_ptr->fontRptText);
	skin_info_ptr->xyRptText = dc.GetTextExtent(strText);
	skin_info_ptr->xyRptText.x /= strText.Length();
	dc.SetFont(skin_info_ptr->fontXText);
	skin_info_ptr->xyXText = dc.GetTextExtent(strText);
	skin_info_ptr->xyXText.x /= strText.Length();
	dc.SetFont(skin_info_ptr->fontYText);
	skin_info_ptr->xyYText = dc.GetTextExtent(strText);
	skin_info_ptr->xyYText.x /= strText.Length();
	skin_info_ptr->xySpace.x = 2;
	skin_info_ptr->xySpace.y = 3;

	skin_info_ptr->xyTabCtrl.x = 18;
	skin_info_ptr->xyTabCtrl.y = 18;
	skin_info_ptr->xyScrollBar.x = 18;
	skin_info_ptr->xyScrollBar.y = 18;
	skin_info_ptr->xyWndIndicator.x = 100;
	skin_info_ptr->xyWndIndicator.y = 100;
	skin_info_ptr->xyInfoIndicator.x = skin_info_ptr->xyXText.x * 5;
	skin_info_ptr->xyInfoIndicator.y = 24;
	skin_info_ptr->xyCoordinate = wxWindow::FromDIP(wxSize(18, 18), dc.GetWindow());

	skin_info_ptr->nBarWidth[0] = 1;
	skin_info_ptr->nBarWidth[1] = 1;
	skin_info_ptr->nBarWidth[2] = 3;
	skin_info_ptr->nBarWidth[3] = 3;
	skin_info_ptr->nBarWidth[4] = 3;
	skin_info_ptr->nBarWidth[5] = 5;
	skin_info_ptr->nBarWidth[6] = 7;
	skin_info_ptr->nBarWidth[7] = 9;
	skin_info_ptr->nBarWidth[8] = 12;
	skin_info_ptr->nBarSpace[0] = 0;
	skin_info_ptr->nBarSpace[1] = 1;
	skin_info_ptr->nBarSpace[2] = 1;
	skin_info_ptr->nBarSpace[3] = 2;
	skin_info_ptr->nBarSpace[4] = 3;
	skin_info_ptr->nBarSpace[5] = 3;
	skin_info_ptr->nBarSpace[6] = 3;
	skin_info_ptr->nBarSpace[7] = 3;
	skin_info_ptr->nBarSpace[8] = 3;
	skin_info_ptr->nBarShift = 0;

	skin_info_ptr->crCtrlText = artProvider->GetColor(wxRIBBON_ART_BUTTON_BAR_LABEL_COLOUR); //平
	skin_info_ptr->crCtrlRising.Set(217, 36, 48/*255, 62, 62*/); //涨
	skin_info_ptr->crCtrlFalling.Set(63, 148, 42/*84, 252, 84*/); //跌

#ifdef _DEBUG
	/*auto bmp = skin_info_ptr->TextToBitmap16(wxT("5S"), wxFontInfo(10).Bold(), skin_info_ptr->crCtrlText);
	skin_info_ptr->SetBitmap16(wxT("5秒"), bmp);
	//skin_info_ptr->Save(bmp);
	bmp.SaveFile(wxT("D:/5.png"), wxBITMAP_TYPE_PNG);*/
#endif
	skin_info_ptr->UpdateTechCycleBitmap16();
#if USE_CYC_SEC
	skin_info_ptr->UpdateTechCycleExBitmap16(CYC_ANYSEC, wxGetApp().GetTechCycleAnySec());
#endif
	skin_info_ptr->UpdateTechCycleExBitmap16(CYC_ANYMIN, wxGetApp().GetTechCycleAnyMin());
	skin_info_ptr->UpdateCustomBitmap32();

	SetSkinInfo(skin_info_ptr);
}
#endif

class MySplashScreen : public wxSplashScreen
{
public:
	using wxSplashScreen::wxSplashScreen;

	virtual int FilterEvent(wxEvent& event) wxOVERRIDE
	{
		const wxEventType t = event.GetEventType();
		auto obj = event.GetEventObject();
		if (obj) {
			if (obj->IsKindOf(wxCLASSINFO(wxWindow))) {
				auto wnd = wxDynamicCast(obj, wxWindow);
				if (wnd == this || wnd == m_window) {
					return wxSplashScreen::FilterEvent(event);
				}
			}
		}
		return Event_Skip;
	}
};

static const wxColour SplashScreenColor = *wxBLACK;
static const wxColour SplashScreenBkColor = *wxWHITE;

// Draws artwork onto our splashscreen at runtime
void MyApp::DecorateSplashScreen(wxBitmap& bmp, bool about)
{
	// use a memory DC to draw directly onto the bitmap
	wxMemoryDC memDc(bmp);

	auto szBmp = bmp.GetSize();
	const wxRect rcBmp(0, 0, szBmp.x, szBmp.y);

	if (about) {
		// draw an orange box (with black outline) at the bottom of the splashscreen.
		// this box will be 10% of the height of the bitmap, and be at the bottom.
		const wxRect bannerRect(wxPoint(0, (szBmp.y / 10) * 2), wxPoint(szBmp.x, (szBmp.y / 10) * 8));
		memDc.SetPen(*wxBLACK_PEN);
		wxDCBrushChanger bc(memDc, wxBrush(SplashScreenBkColor));
		memDc.DrawRectangle(bannerRect);
		//memDc.GradientFillLinear(bannerRect, SplashScreenBkColor, SplashScreenBkColor);
		memDc.DrawLine(bannerRect.GetTopLeft(), bannerRect.GetTopRight());
	}
	else {
		// draw an orange box (with black outline) at the bottom of the splashscreen.
		// this box will be 10% of the height of the bitmap, and be at the bottom.
		const wxRect bannerRect(wxPoint(0, (szBmp.y / 10) * 9), wxPoint(szBmp.x, szBmp.y));
		memDc.SetPen(*wxBLACK_PEN);
		wxDCBrushChanger bc(memDc, wxBrush(SplashScreenBkColor));
		memDc.DrawRectangle(bannerRect);
		//memDc.GradientFillLinear(bannerRect, SplashScreenBkColor, SplashScreenBkColor);
		memDc.DrawLine(bannerRect.GetTopLeft(), bannerRect.GetTopRight());

		// dynamically get the wxWidgets version to display
		wxString description = wxString::Format("" APP_NAME " %s", MYTRADER_VERSION);
		// create a copyright notice that uses the year that this file was compiled
		wxString year(__DATE__);
		wxString copyrightLabel = wxString::Format("%s%s " APP_NAME ". %s",
			wxString::FromUTF8("\xc2\xa9"), year.Mid(year.Length() - 4),
			"All rights reserved.");

		// draw the (white) labels inside of our orange box (at the bottom of the splashscreen)
		//wxDCFontChanger(memDc, wxSystemSettings::GetFont(wxSYS_SYSTEM_FONT)); 
		memDc.SetFont(wxFontInfo(12));
		memDc.SetTextForeground(SplashScreenColor);
		// draw the "wxWidget" label on the left side, vertically centered.
		// note that we deflate the banner rect a little bit horizontally
		// so that the text has some padding to its left.
		memDc.DrawLabel(description, bannerRect.Deflate(5, 0), wxALIGN_CENTRE_VERTICAL | wxALIGN_LEFT);

		// draw the copyright label on the right side
		//memDc.SetFont(wxFontInfo(8));
		memDc.DrawLabel(copyrightLabel, bannerRect.Deflate(5, 0), wxALIGN_CENTRE_VERTICAL | wxALIGN_RIGHT);

		// draw tips
		memDc.DrawLabel(wxT("just a moment, please..."), rcBmp.Deflate(5, 0), wxALIGN_BOTTOM | wxALIGN_LEFT);
	}
}

bool g_wait = false;
bool g_clear_settings = false;
bool g_clear_data = false;
bool g_test = false;

#if !defined(mytraderpro) && !defined(mytraderd)

#if wxUSE_CMDLINE_PARSER

#include <wx/cmdline.h>

bool MyApp::OnCmdLineParsed(wxCmdLineParser& parser)
{
	if (parser.Found(OPTION_WAIT)) {
		g_wait = true;
	}
	if (parser.Found(OPTION_CLEAR_SETTINGS)) {
		g_clear_settings = true;
	}
	if (parser.Found(OPTION_CLEAR_DATA)) {
		g_clear_data = true;
	}
	if (parser.Found(OPTION_TEST)) {
		g_test = true;
	}
	return Base::OnCmdLineParsed(parser);
}

void MyApp::OnInitCmdLine(wxCmdLineParser& parser)
{
	Base::OnInitCmdLine(parser);
	parser.AddLongSwitch(OPTION_TYPE);
	parser.AddLongSwitch(OPTION_WAIT);
	parser.AddLongSwitch(OPTION_CLEAR_SETTINGS);
	parser.AddLongSwitch(OPTION_CLEAR_DATA);
	parser.AddLongSwitch(OPTION_TEST);
}

#endif

bool MyApp::OnInit()
{
    if(!Base::OnInit())
        return false;

#if wxUSE_IMAGE
	wxInitAllImageHandlers();
#endif
//#if wxUSE_LIBPNG
//	wxImage::AddHandler(new wxPNGHandler);
//#endif

	//std::locale::global(std::locale(""));

	mylog::Init();

	SetVendorName("7thtool");
	//SetAppName(APP_NAME); // not needed, it's the default value

	SettingsBase::Init();

	// uncomment this to force writing back of the defaults for all values
	// if they're not present in the config - this can give the user an idea
	// of all possible settings for this program
	//pConfig->SetRecordDefaults();

	std::string app_path = XUtil::get_program_path().string();
	char sha[ZQDB_SHA_DIGEST_LENGTH] = { 0 };
	if (g_test) {
		std::string app_test = app_path + " --test";
		ZQDBSHA1(app_test.data(), app_test.size(), sha);
	} else {
		ZQDBSHA1(app_path.data(), app_path.size(), sha);
	}
	char sha_hex[ZQDB_SHA_DIGEST_LENGTH * 2 + 1] = { 0 };
	ZQDBToHex(sha, ZQDB_SHA_DIGEST_LENGTH, sha_hex);
	snglinst_checker_ = std::make_shared<wxSingleInstanceChecker>(sha_hex);
	if (snglinst_checker_->IsAnotherRunning()) {
		if (g_wait) {
#ifdef _DEBUG
			wxMessageBox("IsAnotherRunning");
#endif
			//snglinst_checker_.reset();
			/*do {
				wxSleep(1);
				snglinst_checker_ = std::make_shared<wxSingleInstanceChecker>(sha_hex);
			} while (snglinst_checker_->IsAnotherRunning());*/
			wxSleep(5);
			auto cmd = wxString::Format(wxT(APP_NAME " --" OPTION_WAIT "%s%s"), g_clear_data ? " --" OPTION_CLEAR_DATA : "", g_clear_settings ? " --" OPTION_CLEAR_SETTINGS : "");
			auto pid = wxExecute(cmd);
			FinalRelease();
			return false;
		}
		else {
			wxMessageBox(wxT(APP_NAME "在运行中..."), wxT("提示"));
			FinalRelease();
			return false;
		}
	}

	//这里保证只有一个mytrader或则mytrader test在运行

	if (g_clear_data) {
#ifdef _DEBUG
		wxMessageBox("ClearData");
#endif
		DoClearData();
	}

	if (g_clear_settings) {
		boost::system::error_code ec;
		boost::filesystem::remove(MySettingsDlg::GetFile(), ec);
	}

	LoadSkinInfo();

#if 0
	wxBitmap bitmap("./res/splash.png", wxBITMAP_TYPE_PNG);
	if (bitmap.IsOk()) {
		// we can even draw dynamic artwork onto our splashscreen
		DecorateSplashScreen(bitmap);
		//skin_info_ptr_->Save(bitmap);
		// show the splashscreen
		new wxSplashScreen(bitmap,
			wxSPLASH_CENTRE_ON_SCREEN | wxSPLASH_TIMEOUT,
			6000, nullptr, wxID_ANY, wxDefaultPosition, wxDefaultSize,
			wxSIMPLE_BORDER | wxSTAY_ON_TOP);
	}
#endif//

	if (!wxTaskBarIcon::IsAvailable())
	{
		wxMessageBox
		(
			"There appears to be no system tray support in your current environment. This sample may not behave as expected.",
			"Warning",
			wxOK | wxICON_EXCLAMATION
		);
	}

	taskbaricon_ = new MyTaskBarIcon();

	// we should be able to show up to 128 characters on Windows
	if (!taskbaricon_->SetIcon(wxICON(mytrader), wxT(APP_NAME)))
	{
		wxLogError("Could not set icon.");
	}

#if defined(__WXOSX__) && wxOSX_USE_COCOA
	dockicon_ = new MyTaskBarIcon(wxTBI_DOCK);
	if (!dockicon_->SetIcon(wxICON(mytrader)))
	{
		wxLogError("Could not set icon.");
	}
#endif

	LoadSettings();

	return true;
}

void MyApp::LoadSettings()
{
	std::string settings_file;
	if (g_test) {
		settings_file = MySettingsDlg::GetFile(g_test);
	}
	else {
		if (!MySettingsDlg::HealthCheck(settings_file)) {
			auto path_module_dir = boost::dll::this_line_location().remove_leaf();
			auto path_desc_file = path_module_dir;
			path_desc_file.append(APP_NAME).append("desc.json");
			auto desc_file = path_desc_file.string();
			CFG_FROM_XML(cfg, desc_file.c_str(), XUtil::XML_FLAG_JSON_FILE);
			MySettingsDlg dlg(cfg);
			if (wxID_OK != dlg.ShowModal()) {
				PostExit();
				return;
			}
			XUtil::json_to_file(settings_file, cfg);
		}
	}

	Init();
}

void MyApp::FinalExit()
{
	Base::Clear();
	if (init_flag_) {
		timer_.Stop();
		if (run_flag_) {
			StopAllStrategy();
			RemoveAllModule();
		}
		size_t stop_flags = ZQDB_STOP_FLAG_NONE;
		if (exit_flag_ & EXIT_FLAG_CLOSE) {
			stop_flags |= ZQDB_STOP_FLAG_CLOSE;
		}
		ZQCalcStop(stop_flags);
		ZQCalcTerm();
	}
	FinalRelease();
}

void MyApp::FinalRelease()
{
	SettingsBase::Clear();

	// clean up: Set() returns the active config object as Get() does, but unlike
	// Get() it doesn't try to create one if there is none (definitely not what
	// we want here!)
	delete wxConfigBase::Set((wxConfigBase *)NULL);

	google::protobuf::ShutdownProtobufLibrary();

	mylog::Term();
}

void MyApp::DoRestart(const wxString& args)
{
	auto cmd = wxString::Format(wxT(APP_NAME " %s"), args);
	auto pid = wxExecute(cmd);
}

void MyApp::DoHide()
{
	MyDownloadDlg::DestroyDlg();
	if (data_frame_) {
		delete data_frame_;
		wxASSERT(!data_frame_);
	}
	auto calc_frames = calc_frames_;
	for (auto frame : calc_frames)
	{
		delete frame;
	}
	wxASSERT(calc_frames_.empty());
	auto frames = tech_frames_;
	for (auto frame : frames) {
		delete frame;
	}
	wxASSERT(tech_frames_.empty());
	if (frame_) {
		delete frame_;
		frame_ = nullptr;
	}
}

#endif

void MyApp::Init()
{
	init_flag_ = true;

	auto busy_info = std::make_shared<wxBusyInfo>(g_test ? wxT("准备超级回测中，请稍后...") : wxT("启动中，请稍后..."));
	
	auto cfg = MySettingsDlg::GetConfig(g_test);
	
	/*if (ZQDBFindData((const char*)settings_file.c_str(), XUtil::XML_FLAG_JSON_FILE)) {
	if (wxOK == wxMessageBox(wxT("以下情况会导致明细、K线等数据缺失：\n")
	wxT("1、程序交易时段关闭过；\n")
	wxT("2、程序运行时崩溃过；\n")
	wxT("3、程序重新设置过。\n")
	wxT("如果您遇到了上述情况，建议选择【确定】清理数据。\n")
	wxT("如果您没有数据缺失问题，请选择默认【取消】正常启动程序。\n")
	wxT("需要清理数据吗？"), wxT("提示"), wxOK | wxCANCEL | wxCANCEL_DEFAULT, nullptr)) {
	ZQDBClearData((const char*)settings_file.c_str(), XUtil::XML_FLAG_JSON_FILE);
	}
	}*/

	auto opt_env = cfg.get_child_optional("env");
	if (opt_env)
	{
		boost::property_tree::ptree &cfg_env = opt_env.get();
		BOOST_FOREACH(const boost::property_tree::ptree::value_type &cfgi, cfg_env)
		{
#ifdef _WIN32
			if (cfgi.first == "PATH") {
				const std::string sys_path = getenv("PATH");
				auto path = cfgi.second.get_value<std::string>();
				//auto add_path = boost::filesystem::complete(cfgi.second.get_value<std::string>()).string();
				//putenv(("PATH=" + path + ";" + add_path).c_str());
				putenv(("PATH=" + path + ";" + sys_path).c_str());
			}
#endif
		}
	}

	std::string json;
	XUtil::json_to_str(json, cfg, false);
	ZQDB_CALC_INF zqdb_inf = { XUtil::XML_FLAG_JSON_STRING, (const char*)json.c_str()
		, [](HNMSG hmsg, size_t* flags)->int
	{
		if (wxGetApp().IsExitFlag()) {
			return 0;
		}
		return wxGetApp().HandleNetMsg(hmsg, flags);
	}
		, [](HZQDB h, ZQDB_NOTIFY_TYPE notify)
	{
		if (wxGetApp().IsExitFlag()) {
			return;
		}
		wxGetApp().HandleNotify(h, notify);
	}
		, [](HZQDB user, HZQDB code, char direction, char offset, char type, double volume, double price, HNMSG* rsp, size_t timeout) -> int
	{
		if (wxGetApp().IsExitFlag()) {
			return 0;
		}
		return wxGetApp().OrderSend(user, code, direction, offset, type, volume, price, rsp, timeout
			, ZQDBGetCalcTradeFlag() ? 0 : ORDER_SEND_FLAG_SHOWTIPS
		);
	}
		, [](HZQDB user, HZQDB order, HNMSG* rsp, size_t timeout) -> int
	{
		if (wxGetApp().IsExitFlag()) {
			return 0;
		}
		return wxGetApp().OrderCancel(user, order, rsp, timeout
			, ZQDBGetCalcTradeFlag() ? 0 : ORDER_SEND_FLAG_SHOWTIPS
		);
	}
		, [](HZQDB user, HZQDB position, char type, double volume, double price, HNMSG* rsp, size_t timeout) -> int
	{
		if (wxGetApp().IsExitFlag()) {
			return 0;
		}
		return wxGetApp().OrderClose(user, position, type, volume, price, rsp, timeout
			, ZQDBGetCalcTradeFlag() ? 0 : ORDER_SEND_FLAG_SHOWTIPS
		);
	}
		//CALCDATA
		, &ZQDB_CALC_DATA_Open
		, &ZQDB_CALC_DATA_Close
		, &ZQDB_CALC_DATA_Update
		, &ZQDB_CALC_DATA_GetUpdateFlags
		, &ZQDB_CALC_DATA_PostUpdate
		, &ZQDB_CALC_DATA_GetFieldValue
		, &ZQDB_CALC_DATA_GetDataMaxCount
		, &ZQDB_CALC_DATA_GetDataCount
		, &ZQDB_CALC_DATA_GetDataValue
	};
	auto rlt = ZQCalcInit(&zqdb_inf);
	if (ZQDB_STATUS_OK != rlt) {
		busy_info.reset();
		if (wxOK == wxMessageBox(wxString::Format(wxT("初始化失败，错误值[%d]\n是否重新设置？"), rlt)
			, wxT("提示"), wxOK | wxCANCEL, nullptr)) {
			Resettings(false);
		}
		//PostExit();
		return;
	}

	UpdateTaskBarInfo(GetAppTitle());

	MatchCH::Init(cfg.get<std::string>("pinyin").c_str());

	Start();
}

void MyApp::Start()
{
	if (ZQDB_STATUS_OK != ZQCalcStart()) {
		PostExit();
		return;
	}

	Bind(wxEVT_TIMER, &MyApp::OnTimer, this);
	timer_.Start(1, true);
}

void MyApp::ShowBusyInfo(const wxString& msg)
{
	if (taskbaricon_) {
		taskbaricon_->ShowBalloon(wxEmptyString, msg);
	}
	else {
		//wxMessageBox(msg, wxT("提示"));
	}
}

void MyApp::UpdateTaskBarInfo(const wxString& info)
{
	wxLogInfo(info);
	if (taskbaricon_) {
		taskbaricon_->SetIcon(wxICON(mytrader), info);
	}
}

void MyApp::PlaySound(const wxString& file)
{
	/*wxSound snd;
	snd.Create(file);
	if (snd.IsOk())
		snd.Play(wxSOUND_ASYNC);*/
	wxSound::Play(file, wxSOUND_ASYNC);
}

bool MyApp::Run()
{
	////同步码表，具体模块再同步对应码表
	//if (ZQDBIsTest()) {
	//}
	//else {
	//	if (ZQDBIsRPC()) {
	//		switch (ZQDBGetAppType())
	//		{
	//		case ZQDB_APP_CLIENT: {
	//			//ShowBusyInfo(wxT("同步市场..."));
	//			for (size_t i = 0, j = ZQDBGetModuleCount(); i < j; i++)
	//			{
	//				if (!ZQDBSyncExchange(ZQDBGetModuleAt(i))) {
	//					ShowBusyInfo(wxT("同步数据失败，程序将退出！！！"));
	//					return false;
	//				}
	//			}
	//			ShowBusyInfo(wxT("同步数据..."));
	//			//ShowBusyInfo(wxT("同步品种..."));
	//			zqdb::AllExchange allexchagne;
	//			for (auto h : allexchagne)
	//			{
	//				ZQDBSyncProduct(h);
	//			}
	//			//ShowBusyInfo(wxT("同步代码..."));
	//			zqdb::AllProduct allproduct;
	//			for (auto h : allproduct)
	//			{
	//				ZQDBSyncCode(h);
	//			}
	//			if (ZQDBIsMiddle()) {
	//				//同步历史数据，中继服务只在第一次同步源数据，后续就和源服务一样通过模块自动更新码表等数据
	//				//ShowBusyInfo(wxT("同步数据...")); 
	//				zqdb::AllCode allcode;
	//				for (auto h : allcode)
	//				{
	//					ZQDBSyncData(h);
	//				}
	//			}
	//			//ShowBusyInfo(wxT("同步数据完成"));
	//		} break;
	//		default: {
	//		} break;
	//		}
	//	}
	//}

	SmartKBManager::Inst().Start();

	ZQDBRun();

	//第一次需要调用一下，这样才会创建计算模块
	ZQDBCalcUpdate(nullptr, ZQDB_NOTIFY_NONE);

	//ZQDBPrevTradeDay(EXCHANGE_CFFEX,20210808, 3);

	for (size_t i = 0, j = ZQDBGetModuleCount(); i < j; i++)
	{
		auto h = ZQDBGetModuleAt(i);
		zqdb::ObjectT<tagZQDBModuleInfo> module(h);
		if (stricmp(module->Code, "ctp") == 0) {
			modules_.emplace_back(std::make_shared<MyCTPModule>(h));
		}
		else if (stricmp(module->Code, "tora") == 0) {
			modules_.emplace_back(std::make_shared<MyTORAModule>(h));
		}
	}

	selfsels_ = wxSplit(GetFilterSelfSel(), wxT(','));
	LoadSelfSel();

	//Post([this] {

	/*//先登录
	MyLoginDlg dlg;
	if (wxID_OK != dlg.ShowModal())
	{
	Exit();
	return;
	}*/

	// Create the main window
	/*smartkbdlg_ = new MySmartKBDlg();
	smartkbdlg_->SetSkinInfo(skin_info_ptr_);
	smartkbdlg_->Show(false);*/

	// Create the main frame window
	Goto(nullptr);

#ifdef _DEBUG
	const char* pysrc = R"(
import numpy
z1 = numpy.zeros((5,6), dtype=float)
z2 = numpy.zeros((4,3), dtype=float)
fill1(z1)
fill2(z2)
print(z1)
print(z2)
config = { 'name':'test', 'value':12312 }
i = 0
def init():
    # 策略初始化运行
    global i
    i = i + 1
    print(config['test'])
def handle(np_bar):
    global i
    i = i + 1
    print(i)
    print(np_bar)
)";
	//zqdb::Calc::AllModule allmodule;
	//HZQDB h = allmodule.back();
	//ZQDBCalcModuleCompileFunc(h, pysrc, [](const char* err, void* data) {
	//}, this);
	/*//auto result = std::async(//std::launch::async|std::launch::deferred,
	std::thread result(
	[h, pysrc] {
	ZQDBUpdateCalc();
	char err[1024] = { 0 };
	auto errlen = ZQDBCalcModuleCompileFunc(h, pysrc, err, 1024);
	});
	result.detach();*/
#endif//
	//});

	return true;
}

int MyApp::OnExit()
{
	FinalExit();
	return Base::OnExit();
}

#ifndef _DEBUG

void MyApp::OnFatalException()
{
	GenerateReport(wxDebugReport::Context_Exception);
}

#endif//

void MyApp::GenerateReport(wxDebugReport::Context ctx)
{
	wxDebugReportCompress *report = new wxDebugReportCompress;

	// add all standard files: currently this means just a minidump and an
	// XML file with system info and stack trace
	report->AddAll(ctx);

	//report->AddFile()
	
	// calling Show() is not mandatory, but is more polite
	if (wxDebugReportPreviewStd().Show(*report)) {
		if (report->Process()) {
#ifdef WIN32
			wxExecute(wxT("explorer.exe /select,") + report->GetCompressedFileName());
#else
			wxLaunchDefaultBrowser(report->GetCompressedFileName());
#endif//
			//wxLogMessage("Report generated in \"%s\".",
			//	report->GetCompressedFileName());
		}
		else {
			wxLaunchDefaultBrowser(report->GetDirectory());
		}
		report->Reset();
	}
	//else: user cancelled the report

	delete report;
}

void MyApp::Exit(size_t flag)
{
#ifndef _DEBUG
	if (init_flag_) {
		if (run_flag_) {
			if (wxOK != wxMessageBox(wxT("确定要退出程序吗？"), wxT("提示"), wxOK | wxCANCEL, nullptr)) {
				return;
			}
		}
	}
#endif
	{
		DoExit(flag);
	}
}

void MyApp::DoExit(size_t flag)
{
	exit_flag_ = flag;
	SmartKBManager::Inst().Stop();
	DoHide();
	if (smartkbdlg_) {
		delete smartkbdlg_;
		wxASSERT(smartkbdlg_ == nullptr);
	}
	if (taskbaricon_) {
		//taskbaricon_->PopEventHandler(true);
		//taskbaricon_->Destroy();
		delete taskbaricon_;
		taskbaricon_ = nullptr;
	}
}

void  MyApp::PostExit(size_t flag)
{
	Post(50, [this, flag] {
		DoExit(flag);
	});
}

wxString MyApp::GetAppMode()
{
	switch (ZQDBGetAppType())
	{
	case ZQDB_APP_STANDALONE: {
		return wxT("单机");
	} break;
	case ZQDB_APP_SERVER: {
		return wxT("服务器");
	} break;
	case ZQDB_APP_CLIENT: {
		return wxT("超级终端");
	} break;
	}
	return wxEmptyString;
}

wxString MyApp::GetAppTitle()
{
	wxString strAppTitle = wxString(wxT(APP_TITLE)) + wxT(" ") + GetAppMode() + wxT(" - ");
	switch (ZQDBGetAppRunMode())
	{
	case ZQDB_APP_RUN_REAL: {
		strAppTitle += wxT("实盘");
	}
	break;
	case ZQDB_APP_RUN_MOCK: {
		strAppTitle += wxT("模拟盘");
	}
	break;
	case ZQDB_APP_RUN_TEST: {
		strAppTitle += wxT("超级回测");
	}
	break;
	default: {
		wxASSERT(0);
	} break;
	}
	return strAppTitle;
}

wxString MyApp::GetAppStatus(bool ex)
{
	wxString strAppStatus = GetAppTitle();
	if (ZQDBIsTest()) {
		//
	}
	else {
		if (ZQDBClient()) {
			strAppStatus += wxT(" - ");
			if (!ZQDBIsConnect()) {
				strAppStatus += wxT("连接中");
			}
			else {
				ZQDB_MSG* hmsg = nullptr;
				if (ZQDBIsLogin(&hmsg)) {
					zqdb::Msg msg(hmsg, zqdb::Msg::AutoDelete);
					if (!msg.IsError()) {
						strAppStatus += wxT("在线");
					}
					else {
						strAppStatus += wxT("登录失败");
					}
				}
				else {
					strAppStatus += wxT("已连接");
				}
			}
		}
		if (ZQDBServer()) {
			strAppStatus += wxT(" - ");
			if (ZQDBIsIPCServer()) {
				if (ex) {
					strAppStatus += wxString::Format(wxT("IPC正常[%zu]"), ZQDBGetIPCCount());
				}
				else {
					strAppStatus += wxT("IPC正常");
				}
			}
			else {
				strAppStatus += wxString::Format(wxT("IPC停止"));
			}
			strAppStatus += wxT(",");
			if (ZQDBIsRPCServer()) {
				if (ex) {
					strAppStatus += wxString::Format(wxT("RPC正常[%zu]"), ZQDBGetRPCCount());
				}
				else {
					strAppStatus += wxT("RPC正常");
				}
			}
			else {
				strAppStatus += wxString::Format(wxT("RPC停止"));
			}
		}
	}
	return strAppStatus;
}

wxDialog* MyApp::AnyModalDlg()
{
	if (!modaldlgs_.empty()) {
		return modaldlgs_.back();
	}
	return nullptr;
}

bool MyApp::RemoveModalDlg(wxDialog* dlg)
{
	auto it = std::find(modaldlgs_.begin(), modaldlgs_.end(), dlg);
	if (it != modaldlgs_.end()) {
		modaldlgs_.erase(it);
		return true;
	}
	return false;
}

void MyApp::AddModalDlg(wxDialog* dlg)
{
	modaldlgs_.push_back(dlg);
}

wxFrame* MyApp::GetFrame()
{ 
	return frame_; 
}

bool MyApp::FindTechFrame(wxFrame * frame)
{
	if (frame == frame_) {
		return true;
	}
	auto it = std::find(tech_frames_.begin(), tech_frames_.end(), frame);
	if (it != tech_frames_.end()) {
		return true;
	}
	return false;
}

void MyApp::ShowFrame(wxFrame* frame)
{
	if (!frame) {
		frame = GetFrame();
	}
	if (!frame) {
		Goto(nullptr);
	} else {
		if (frame->IsIconized()) {
			frame->Restore();
		}
		//if (!frame->IsVisible()) {
		frame->Show();
		//}
		frame->Raise();
	}
}

void MyApp::ResetFrame(wxFrame * frame)
{
	frame_->RemoveFrame((MyBaseFrame*)frame);
	auto it = std::find(tech_frames_.begin(), tech_frames_.end(), frame);
	if (it != tech_frames_.end()) {
		tech_frames_.erase(it);
	}
}

void MyApp::Goto(HZQDB h, wxWindow* top)
{
	MyTechFrame* frame = (MyTechFrame*)top;
	if (!frame) {
		if (!frame_) {
			frame_ = new MyFrame("./" APP_NAME "/techfrm.json", XUtil::XML_FLAG_JSON_FILE);
			frame_->SetTitle(GetAppTitle());
			frame_->SetSkinInfo(skin_info_ptr_);
			auto info_ptr = std::make_shared<zqdb::TechContainerInfo>();
			info_ptr->Set(h);
			frame_->SetInfo(info_ptr);
			frame_->Maximize();
			frame = frame_;
		}
		else {
			auto techframe = new MyTechFrame("./" APP_NAME "/techfrm.json", XUtil::XML_FLAG_JSON_FILE);
			techframe->SetSkinInfo(skin_info_ptr_);
			auto info_ptr = std::make_shared<zqdb::TechContainerInfo>();
			info_ptr->Set(h);
			techframe->SetInfo(info_ptr);
			frame_->AddFrame(techframe);
			tech_frames_.emplace_back(techframe);
			frame = techframe;
		}
		frame->SetStatusText(last_tips_);
	}
	else {
		ASSERT(FindTechFrame(frame));
		frame->Goto(h);
	}
	ShowFrame(frame);
}

int MyApp::ShowCalcFuncDlg(wxWindow* parent, CALC_TYPE type, const char* name)
{
	CalcFuncDlg dlg(parent, type, name);
	return dlg.ShowModal();
}

void MyApp::ResetCalcFrame(MyCalcFrame* frame)
{
	//calc_frame_ = nullptr;
	auto it = std::find(calc_frames_.begin(), calc_frames_.end(), frame);
	if (it != calc_frames_.end()) {
		calc_frames_.erase(it);
	}
}

/*void MyApp::ShowCalcFrame()
{
	if (!calc_frame_) {
		calc_frame_ = new MyCalcFrame("./" APP_NAME "/techfrm.json", XUtil::XML_FLAG_JSON_FILE);
		//calc_frame_->SetSkinInfo(skin_info_ptr_);
		calc_frame_->SetInfo(info_ptr);
	}
	if (calc_frame_->IsIconized()) {
		calc_frame_->Restore();
	}
	//if (!calc_frame_->IsVisible()) {
		calc_frame_->Show();
	//}
	calc_frame_->Raise();
}*/

void MyApp::ShowCalcFrame(std::shared_ptr<zqdb::FuncContainerInfo> info_ptr)
{
	auto calc_frame = new MyCalcFrame(nullptr, XUtil::XML_FLAG_JSON_STRING);
	calc_frame->SetSkinInfo(skin_info_ptr_);
	calc_frame->SetInfo(info_ptr);
	calc_frames_.emplace_back(calc_frame);

	calc_frame->Show();
	calc_frame->Raise();
	calc_frame->CenterOnScreen();
}

void MyApp::ResetStrategyFrame(MyStrategyFrame * frame)
{
	auto it = std::find(strategy_frames_.begin(), strategy_frames_.end(), frame);
	if (it != strategy_frames_.end()) {
		strategy_frames_.erase(it);
	}
}

void MyApp::ShowPreferencesEditor(wxWindow* parent, size_t flags)
{
	if (!parent) {
		parent = frame_;
	}
	if (!m_prefEditor)
	{
		m_prefEditor.reset(new wxPreferencesEditor);
		m_prefEditor->AddPage(new PrefsPageGeneral());
		m_prefEditor->AddPage(new PrefsPageAbout());
	}

	m_prefEditor->Show(parent);
}

void MyApp::DismissPreferencesEditor()
{
	if (m_prefEditor)
		m_prefEditor->Dismiss();
}

void MyApp::Resettings(bool showMsg)
{
	if (!showMsg || wxOK == wxMessageBox(wxT("重新设置将会重启程序并弹出第一次运行时设置对话框。\n")
		wxT("确定要重新设置吗？"), wxT("提示"), wxOK | wxCANCEL, nullptr)) {
		Post(50, [this] {
			DoExit();
			DoRestart(wxT("--" OPTION_WAIT " --" OPTION_CLEAR_DATA " --" OPTION_CLEAR_SETTINGS));
		});
	}
}

void MyApp::ResetDataFrame()
{
	data_frame_ = nullptr;
}

void MyApp::DownloadData()
{
	MyDownloadDlg::ShowDlg();
	/*if (!data_frame_) {
		data_frame_ = new MyDataFrame(nullptr, XUtil::XML_FLAG_JSON_STRING);
	}
	data_frame_->Show();*/
}

void MyApp::ExportData()
{
	wxFileDialog dlg(nullptr,
		wxT("导出数据"),
		wxEmptyString,
		wxT("mydata.zip"),
		"ZIP压缩文件(*.zip)|*.zip",
		wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (dlg.ShowModal() == wxID_OK) {
		auto data_dir = utf2wxString(ZQDBGetDataDir());
		wxString cmd = wxString::Format(wxT("myarchive c %s"), dlg.GetPath());
		auto src_dir = utf2wxString(ZQDBGetSrcDir());
		if (wxDirExists(src_dir)) {
			cmd += " " + src_dir;
		}
		if (wxDirExists(data_dir + "/TORA")) {
			cmd += " TORA";
		}
		if (wxDirExists(data_dir + "/ctp")) {
			cmd += " ctp";
		}
		wxExecuteEnv env;
		env.cwd = data_dir;
		int code = wxExecute(cmd, wxEXEC_SYNC, nullptr, &env);
		if (code == 0) {
			wxMessageBox(wxT("导出完成！！！"));
		}
		else {
			wxMessageBox(wxString::Format(wxT("导出失败，错误值=%d"), code));
		}
	}
}

void MyApp::SaveClose()
{
	auto allmodule = AllModule();
	wxArrayString choices;
	for (auto module : allmodule)
	{
		choices.push_back(utf2wxString((*module)->Name));
	}
	wxArrayInt selections;
	auto count = wxGetSelectedChoices(selections
		, wxT("请选择强制收盘的模块（交易中请不要使用该功能）"), wxT(APP_NAME)
		, choices);
	if (count > 0) {
		std::vector<HZQDB> hs;
		for (int n = 0; n < count; n++)
		{
			hs.push_back(*allmodule[selections[n]]);
		}
		{
			wxBusyInfo busy(wxT("正在收盘，请稍后..."));
			ZQDBSaveClose(hs.data(), hs.size());
		}
		wxMessageBox(wxT("收盘完成"),
			wxT("提示"), wxOK | wxICON_INFORMATION);
	}
}

void MyApp::DoClearData()
{
	//清理数据
	ZQDBClearData((const char*)MySettingsDlg::GetFile().c_str(), XUtil::XML_FLAG_JSON_FILE);
}

void MyApp::ClearData(bool showMsg)
{
	if (!showMsg || ZQDBIsRPC() || wxOK == wxMessageBox(wxT("清理数据需要关闭并重启程序。\n")
		wxT("确定要清理数据吗？"), wxT("提示"), wxOK | wxCANCEL, nullptr)) {
		Post(50, [this] {
			DoExit();
			DoRestart(wxT("--" OPTION_WAIT " --" OPTION_CLEAR_DATA));
		});
	}
}

void MyApp::Restart(bool showMsg)
{
	if (!showMsg || wxOK == wxMessageBox(wxT("确定要重启程序吗？"), wxT("提示"), wxOK | wxCANCEL, nullptr)) {
		Post(50, [this] {
			DoExit();
			DoRestart(wxT("--" OPTION_WAIT));
		});
	}
}

void MyApp::Test(size_t begin, size_t end, size_t speed)
{
	wxASSERT(!ZQDBIsTest());
	//if (wxOK == wxMessageBox(wxString::Format("确定开始超级回测吗？"), wxT("提示"), wxOK | wxCANCEL, nullptr)) {
		try {
			boost::system::error_code ec;
			boost::filesystem::path test_file(MySettingsDlg::GetFile(true));
			if (boost::filesystem::exists(test_file)) {
				boost::filesystem::remove(test_file);
			}
			boost::filesystem::copy(MySettingsDlg::GetFile(), test_file, ec);

			CFG_FROM_XML(cfg, test_file.string().c_str(), XUtil::XML_FLAG_JSON_FILE);
			cfg.put("mode", (int)ZQDB_APP_RUN_TEST);
			cfg.put("test.begin", begin);
			cfg.put("test.end", end);
			cfg.put("test.speed", speed);
			cfg.put("mdb.name", "test.mdb");
			XUtil::json_to_file(test_file, cfg);

			//Post(50, [this] {
			auto cmd = wxString::Format(wxT(APP_NAME " --test"));
			auto pid = wxExecute(cmd);
			//});
			return;
		} catch (const std::exception& ex) {
		}
		wxMessageBox(wxT("启动超级回测失败!!!"), wxT("提示"), wxOK);
	//}
}

void MyApp::ShowAbout(wxWindow* parent)
{
	MyAboutDlg dlg(parent);
	dlg.ShowModal();
	return;
	wxBitmap bitmap("./res/splash.png", wxBITMAP_TYPE_PNG);
	if (bitmap.IsOk()) {
		// we can even draw dynamic artwork onto our splashscreen
		//DecorateSplashScreen(bitmap, true);
		//skin_info_ptr_->Save(bitmap);
		// show the splashscreen
		wxSplashScreen *splash = new MySplashScreen(bitmap,
			wxSPLASH_CENTRE_ON_SCREEN | wxSPLASH_NO_TIMEOUT,
			6000, parent, wxID_ANY, wxDefaultPosition, wxDefaultSize,
			/*wxSIMPLE_BORDER | */wxSTAY_ON_TOP);

		wxWindow *win = splash->GetSplashWindow();

		wxSizer *sizer = new wxBoxSizer(wxVERTICAL);
		sizer->AddStretchSpacer(1);

		auto stc_ver = new wxStaticText(win, wxID_ANY, wxString::Format(wxT("" APP_NAME " v%s"), MYTRADER_VERSION));
		stc_ver->SetForegroundColour(SplashScreenColor);
		stc_ver->SetBackgroundColour(SplashScreenBkColor);
		sizer->Add(stc_ver, wxSizerFlags().Center());
		//wxListBox *box = new wxListBox(this, wxID_ANY);
		//box->SetMinSize(wxSize(400, 300));
		//sizer->Add(box, wxSizerFlags(1).Border().Expand());
		//logo
		auto link_mytrader = new wxGenericHyperlinkCtrl(win, wxID_ANY, wxT("www.mytrader.org.cn"), wxT("www.mytrader.org.cn"));
		link_mytrader->SetBackgroundColour(SplashScreenBkColor);
		sizer->Add(link_mytrader, wxSizerFlags().Center());
		//qq
		//help
		auto link_help_doc = new wxGenericHyperlinkCtrl(win, wxID_ANY, _("help docs"), wxT("https://gitee.com/7thTool/mytrader"));
		link_help_doc->SetBackgroundColour(SplashScreenBkColor);
		sizer->Add(link_help_doc, wxSizerFlags().Center());
		//src
		auto link_mytrader_src = new wxGenericHyperlinkCtrl(win, wxID_ANY, wxT("https://gitee.com/7thTool/mytrader"), wxT("https://gitee.com/7thTool/mytrader"));
		link_mytrader_src->SetBackgroundColour(SplashScreenBkColor);
		sizer->Add(link_mytrader_src, wxSizerFlags().Center());
		auto link_zqdb_src = new wxGenericHyperlinkCtrl(win, wxID_ANY, wxT("https://gitee.com/7thTool/zqdb"), wxT("https://gitee.com/7thTool/zqdb"));
		link_zqdb_src->SetBackgroundColour(SplashScreenBkColor);
		sizer->Add(link_zqdb_src, wxSizerFlags().Center());

		sizer->AddStretchSpacer(1);

		//auto btn_ok = new wxButton(win, wxID_OK, _("&OK"));
		//auto btn_cancel = new wxButton(this, wxID_CANCEL, _("&Cancel"));

		//wxBoxSizer *sizerBottom = new wxBoxSizer(wxHORIZONTAL);

		//sizerBottom->AddStretchSpacer(1);
		//sizerBottom->Add(btn_ok, 0, wxALIGN_CENTER | wxALL, 5);
		//sizerBottom->Add(btn_cancel, 0, wxALIGN_CENTER | wxALL, 5);
		//sizerBottom->AddStretchSpacer(1);

		//sizer->Add(sizerBottom, 0, wxEXPAND);

		win->SetSizer(sizer);
		win->Layout();

		/*wxStaticText *text = new wxStaticText(win,
			wxID_EXIT,
			"click somewhere\non this image",
			wxPoint(13, 11)
		);
		text->SetBackgroundColour(*wxWHITE);
		text->SetForegroundColour(*wxBLACK);
		wxFont font = text->GetFont();
		font.SetFractionalPointSize(2.0*font.GetFractionalPointSize() / 3.0);
		text->SetFont(font);*/
	}
}

void MyApp::LoadSelfSel()
{
	for (auto it = selfsels_.begin(); it != selfsels_.end();)
	{
		const auto& sub = *it;
		if (!sub.empty()) {
			auto h = ZQDBGetCode(wxString2utf(sub).c_str(), nullptr);
			if (h) {
				ZQDBSetFlags(h, ZQDBGetFlags(h) | ZQDB_CODE_FLAG_SELFSEL);
				ZQDBSubscribeMarketData(h);
				it = selfsels_.erase(it);
			}
			else {
				++it;
			}
		}
		else {
			++it;
		}
	}
}

void MyApp::SaveSelfSel()
{
	std::ostringstream ss;
	zqdb::AllCode allcode;
	for (auto h : allcode)
	{
		if (ZQDBGetFlags(h) & ZQDB_CODE_FLAG_SELFSEL) {
			zqdb::Code code(h);
			char szCode[260] = { 0 };
			ZQDBCodeCombine(code->Exchange, code->Code, szCode);
			ss << szCode << ",";
		}
	}
	ss.seekp(-1, ss.cur);
	SetFilterSelfSel(utf2wxString(ss.str().c_str()));
}

bool MyApp::IsSelfSel(HZQDB h)
{
	if (ZQDBGetFlags(h) & ZQDB_CODE_FLAG_SELFSEL) {
		return true;
	}
	return false;
}

void MyApp::AddSelfSel(HZQDB h, bool save)
{
	ZQDBSetFlags(h, ZQDBGetFlags(h) | ZQDB_CODE_FLAG_SELFSEL);
	ZQDBSubscribeMarketData(h);
	if (save) {
		SaveSelfSel();
	}
}

void MyApp::RemoveSelfSel(HZQDB h, bool save)
{
	ZQDBSetFlags(h, ZQDBGetFlags(h) & ~ZQDB_CODE_FLAG_SELFSEL);
	ZQDBUnSubscribeMarketData(h);
	if (save) {
		SaveSelfSel();
	}
}

void MyApp::Subscribe(HZQDB h)
{
	wxASSERT(h);
	subs_[h] += 1;
	if (subs_[h] == 1) {
		ZQDBSubscribeMarketData(h);
	}
}

void MyApp::UnSubscribe(HZQDB h)
{
	if (!h) {
		return;
	}
	auto it = subs_.find(h);
	if (it != subs_.end()) {
		if (it->second > 1) {
			it->second -= 1;
		}
		else {
			subs_.erase(it);
			ZQDBUnSubscribeMarketData(h);
		}
	}
}

bool MyApp::IsSubscribe(HZQDB h)
{
	return subs_.find(h) != subs_.end();
}

void MyApp::OnTimer(wxTimerEvent& evt)
{
	if (!run_flag_) {
		if (ZQDBIsTest()) {
			run_flag_ = true;
		}
		else {
			/*if (ZQDBIsRPC()) {
				int err = 0;
				if (ZQDBIsLogin(&err)) {
					run_flag_ = err == ZQDB_STATUS_OK;
				}
			}
			else */{
				run_flag_ = true;
			}
		}
		if (run_flag_) {
			if (!Run()) {
				DoExit();
			}
		}
		else {
			//if (evt.GetInterval() < 1000) {
			//	//first
			//	if (ZQDBIsRPC()) {
			//		ShowBusyInfo(wxT("正在连接服务器..."));
			//	}
			//}
		}
	}
	else {
		run_flag_++;

		//强制周末重启一次
		if (!ZQDBIsServer()) 
		{
			auto now = wxDateTime::Now();
			if (now.GetWeekDay() == wxDateTime::WeekDay::Sun) {
				if (now.GetHour() == 17 && now.GetMinute() < 1 && now.GetSecond() < 3) {
					Restart(false);
					return;
				}
			}
		}

//#if 0
//		const std::array<uint32_t, 1> close_time = { 172500 }, close_end_time = { 17300 }, max_close_end_time = { 173500 };
//#else
//		const std::array<uint32_t, 2> close_time = { 85400, 205400 }, close_end_time = { 85500, 205500 }, max_close_end_time = { 90000, 210000 };
//#endif//
//		if (ZQDBIsServer()) {
//			//每天close_time暂停服务到close_end_time
//			//if (ZQDBIsListen()) {
//			if (ZQDBIsIPCServer() && ZQDBIsRPCServer()) {
//				uint32_t date = 0, time = 0;
//				date = XUtil::NowDateTime(&time);
//				for (size_t i = 0; i < close_time.size(); i++)
//				{
//					if (time > close_time[i] && time < close_end_time[i]) {
//						//ZQDBStopListen();
//						ZQDBPauseServer(true, true);
//					}
//				}
//			}
//		}
//		if (suspend_flag_) {
//			if (ZQDBIsServer()) {
//				if (ZQDBIsListen()) {
//					ASSERT(0);
//					suspend_flag_ = 0;
//				}
//				else {
//					//每天close_end_time到max_close_end_time之间服务断开超过60秒且所有市场都可用就自动重启服务
//					uint32_t date = 0, time = 0;
//					date = XUtil::NowDateTime(&time);
//					for (size_t i = 0; i < close_time.size(); i++)
//					{
//						if (time > close_end_time[i] && time < max_close_end_time[i]) {
//							if (!ZQDBIsAnyDisabledModule()) {
//								if ((run_flag_ - suspend_flag_) > 60) {
//									ZQDBStartListen();
//								}
//							}
//						}
//					}
//				}
//			} else {
//				//每天close_end_time到max_close_end_time之间连接断开超过30秒就需要重新启动程序
//				if ((run_flag_ - suspend_flag_) > 30) {
//					uint32_t date = 0, time = 0;
//					date = XUtil::NowDateTime(&time); 
//					for (size_t i = 0; i < close_time.size(); i++)
//					{
//						if (time > close_time[i] && time < max_close_end_time[i]) {
//							Restart(false);
//							return;
//						}
//					}
//				}
//			}
//		}
//		else {
			////非挂起状态，自动启服务
			//if (ZQDBIsServer()) {
			//	if (!ZQDBIsAnyDisabledModule()) {
			//		if (ZQDBIsListen()) {
			//			//if (!ZQDBIsIPCServer() || !ZQDBIsRPCServer()) {
			//			//	//每天close_end_time到max_close_end_time之间服务断开超过60秒且所有市场都可用就自动重启服务
			//			//	uint32_t date = 0, time = 0;
			//			//	date = XUtil::NowDateTime(&time);
			//			//	for (size_t i = 0; i < close_time.size(); i++)
			//			//	{
			//			//		if (time > close_end_time[i] && time < max_close_end_time[i]) {
			//			//			if ((run_flag_ - suspend_flag_) > 60) {
			//			//				ZQDBRestartServer(true, true);
			//			//			}
			//			//		}
			//			//	}
			//			//}
			//		}
			//		else {
			//			ZQDBStartListen();
			//		}
			//	}
			//}
//		}

		ZQDBCalcUpdateTimer();
		BroadcastModule([](std::shared_ptr<zqdb::Module> module)->bool {
			auto mymodule = std::dynamic_pointer_cast<MyModule>(module);
			mymodule->OnTimer();
			return false;
		});
		if (frame_) {
			auto title = GetAppStatus(true);
			if (title != frame_->GetTitle()) {
				frame_->SetTitle(title);
			}
			frame_->GetEventHandler()->ProcessEvent(evt);
		}
		for (auto frame : tech_frames_) {
			frame->GetEventHandler()->ProcessEvent(evt);
		}
	}
	//
	timer_.Start(1000, true);
}

void MyApp::OnNotifyStatus(HZQDB h) 
{
	switch (h->type)
	{
	case ZQDB_HANDLE_TYPE_CLIENT: {
		ZQDB_MSG* hmsg = nullptr;
		if (ZQDBIsLogin(&hmsg)) {
			zqdb::Msg msg(hmsg, zqdb::Msg::AutoDelete);
			int err = msg.IsError();
			if (!err) {
			}
			else {
				static zqdb::TaskID dealy;
				if (dealy) {
					Cancel(dealy);
				}
				dealy = Post(1000, [this,err]() {
					wxString strTips;
					switch (err)
					{
					case ZQDB_STATUS_EACCES: {
						strTips = wxT("认证失败，请重新登录！！！");
					} break;
					case ZQDB_STATUS_IN_USE: {
						strTips = wxT("已登录，请不要重复登录！！！");
					} break;
					case ZQDB_STATUS_INIT: {
						Post(60*1000, [this] {
							if (ZQDBIsClient()) {
								Restart(false);
							}
							else {
								//重登陆
								//ZQDBReconnect();
							}
						});
					} break;
					case ZQDB_STATUS_VERSION: {
						strTips = wxT("版本不匹配，请重启升级或者下载最新版本！！！");
					} break;
					default: {
						strTips = wxString::Format(wxT("登录失败[%d]，请重新登录！！！"), err);
					} break;
					}
					if (!strTips.IsEmpty()) {
						if (wxOK == wxMessageBox(strTips, wxT("提示"), wxOK, nullptr)) {
							//DoExit();
						}
					}
				});
			}
		}
	} 
	case ZQDB_HANDLE_TYPE_SERVER: {
		if (!ZQDBIsDisable(h)) {
			suspend_flag_ = 0; //清空连接断开标志
		}
		else {
			suspend_flag_ = run_flag_; //记录停止服务标志
		}
		UpdateTaskBarInfo(GetAppStatus());
	} break;
	case ZQDB_HANDLE_TYPE_EXCHANGE: {
		if (!ZQDBIsDisable(h)) {
			//加载订阅列表
			LoadSelfSel();
		}
	} break;
	case ZQDB_HANDLE_TYPE_MODULE: {
		if (ZQDBIsDisable(h)) {
			SmartKBManager::Inst().ClearAll();
		}
		else {
			SmartKBManager::Inst().UpdateAll();
		}
	} break;
	default: {
	} break;
	}
	BroadcastModule([h](std::shared_ptr<zqdb::Module> module)->bool {
		module->OnNotifyStatus(h);
		return false;
	});
	if (MyLoginDlg::Inst()) {
		MyLoginDlg::Inst()->OnNotifyStatus(h);
	}
	if (smartkbdlg_) {
		smartkbdlg_->OnNotifyStatus(h);
	}
	if (frame_) {
		frame_->OnNotifyStatus(h);
	}
	for (auto frame : tech_frames_) {
		frame->OnNotifyStatus(h);
	}
	for (auto frame : calc_frames_) {
		frame->OnNotifyStatus(h);
	}
}
void MyApp::OnNotifyAdd(HZQDB h)
{
	switch (h->type)
	{
	case ZQDB_HANDLE_TYPE_TABLE_DATA: {
		HZQDB hcodes[3] = { 0 };
		if (ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_ORDER)) {
			if (!GetCloseSound()) {
				PlaySound(wxT("./sound/Accepted.wav"));
			}
			zqdb::ObjectT<tagOrderInfo> order(h);
			ZQDBGetCodeByTradeCode(ZQDBGetModule(h), order->Code, hcodes, 3);
		}
		else if (ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_TRADE)) {
			if (!GetCloseSound()) {
				PlaySound(wxT("./sound/Trade.wav"));
			}
			zqdb::ObjectT<tagTradeInfo> trade(h);
			ZQDBGetCodeByTradeCode(ZQDBGetModule(h), trade->Code, hcodes, 3);
		}
		else if (ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_POSITION)) {
			zqdb::ObjectT<tagPositionInfo> position(h);
			ZQDBGetCodeByTradeCode(ZQDBGetModule(h), position->Code, hcodes, 3);
		}
		for (auto hcode : hcodes) {
			if (hcode) {
				ZQDBSubscribeMarketData(hcode);
			}
		}
	} break;
	default:
		break;
	}
	BroadcastModule([h](std::shared_ptr<zqdb::Module> module)->bool {
		module->OnNotifyAdd(h);
		return false;
	});
	if (MyLoginDlg::Inst()) {
		MyLoginDlg::Inst()->OnNotifyAdd(h);
	}
	if (smartkbdlg_) {
		smartkbdlg_->OnNotifyAdd(h);
	}
	if (frame_) {
		frame_->OnNotifyAdd(h);
	}
	for (auto frame : tech_frames_) {
		frame->OnNotifyAdd(h);
	}
	for (auto frame : calc_frames_) {
		frame->OnNotifyAdd(h);
	}
}
void MyApp::OnNotifyRemove(HZQDB h)
{
	BroadcastModule([h](std::shared_ptr<zqdb::Module> module)->bool {
		module->OnNotifyRemove(h);
		return false;
	});
	if (MyLoginDlg::Inst()) {
		MyLoginDlg::Inst()->OnNotifyRemove(h);
	}
	if (smartkbdlg_) {
		smartkbdlg_->OnNotifyRemove(h);
	}
	if (frame_) {
		frame_->OnNotifyRemove(h);
	}
	for (auto frame : tech_frames_) {
		frame->OnNotifyRemove(h);
	}
	for (auto frame : calc_frames_) {
		frame->OnNotifyRemove(h);
	}
}
void MyApp::OnNotifyUpdate(HZQDB h)
{
	BroadcastModule([h](std::shared_ptr<zqdb::Module> module)->bool {
		module->OnNotifyUpdate(h);
		return false;
	});
	if (MyLoginDlg::Inst()) {
		MyLoginDlg::Inst()->OnNotifyUpdate(h);
	}
	if (smartkbdlg_) {
		smartkbdlg_->OnNotifyUpdate(h);
	}
	if (frame_) {
		frame_->OnNotifyUpdate(h);
	}
	for (auto frame : tech_frames_) {
		frame->OnNotifyUpdate(h);
	}
	for (auto frame : calc_frames_) {
		frame->OnNotifyUpdate(h);
	}
}

void MyApp::HandleNotify(HZQDB h, ZQDB_NOTIFY_TYPE notify)
{
	switch (h->type)
	{
	case ZQDB_HANDLE_TYPE_CATEGORY: {
		ZQDBRefCategory(h); //分类通知对象需要增加引用计数，以防止对象失效
	} break;
	case ZQDB_HANDLE_TYPE_CALCFUNC: {
		ZQDBCalcRef(h); //计算模块通知对象需要增加引用计数，以防止对象失效
	} break;
	}
	Post([this, h, notify]() {
		bool calc_update = true, notify_update = true;
		switch (notify)
		{
		case ZQDB_NOTIFY_STATUS: {

		} break;
		case ZQDB_NOTIFY_ADD: {

		} break;
		case ZQDB_NOTIFY_REMOVE: {

		} break;
		case ZQDB_NOTIFY_UPDATE: {
			switch (h->type)
			{
			case ZQDB_HANDLE_TYPE_CODE: {
				calc_update = ZQDBCalcIsSubscribe(h);
				notify_update = IsSubscribe(h);
			} break;
			}
		} break;
		default:
			break;
		}
		if (calc_update) {
			ZQDBCalcUpdate(h, notify);
		}
		if (notify_update) {
			OnNotify(h, notify);
		}
		//处理后再资源释放
		switch (h->type)
		{
		case ZQDB_HANDLE_TYPE_CATEGORY: {
			ZQDBCloseCategory(h);
		} break;
		case ZQDB_HANDLE_TYPE_CALCFUNC: {
			ZQDBCloseCalcFunc(h);
		} break;
		}
	});
}

//void MyApp::HandleNetStatus(HNNODE h, ZQDB_NODE_STATUS status)
//{
//	//Post(std::bind(&MyApp::OnNetStatus, this, h, status));
//	if (ZQDBIsServer()) {
//		if (h == ZQDBGetSrvNode()) {
//			switch (status)
//			{
//			case ZQDB_NODE_STATUS_DISCONNECT: {
//				Post(std::bind(&MyApp::OnNotifyStopListen, this));
//			} break;
//			case ZQDB_NODE_STATUS_CONNECTED: {
//				Post(std::bind(&MyApp::OnNotifyStartListen, this));
//			} break;
//			default:
//				break;
//			}
//		}
//	}
//	else {
//		if (h == ZQDBGetCliNode()) {
//			switch (status)
//			{
//			case ZQDB_NODE_STATUS_DISCONNECT: {
//				Post(std::bind(&MyApp::OnNotifyDisconnect, this));
//			} break;
//			case ZQDB_NODE_STATUS_CONNECTED: {
//				Post(std::bind(&MyApp::OnNotifyConnect, this));
//			} break;
//			default:
//				break;
//			}
//		}
//	}
//}

int MyApp::HandleNetMsg(HNMSG hmsg, size_t* flags)
{
	//zqdb::Msg msg(hmsg, zqdb::Msg::Ref);
	//Post(std::bind(&MyApp::OnNetMsg, this, msg));
	return 0;
}

void MyApp::ShowTips(int level, const char* xml, size_t xmlflag)
{
	ShowTips(level, FormatTips(xml, xmlflag));
}

void MyApp::ShowTips(int level, wxString&& str)
{
	last_level_ = level;
	last_tips_ = std::move(str);
	/*if (MyLoginDlg::Inst())
	{
		auto event = new wxStringCommandEvent(logstr, ZQDB_NOTIFY_LOG_UPDATE_EVENT);
		wxQueueEvent(MyLoginDlg::Inst()->GetEventHandler(), event);
	}*/
	if (frame_) {
		frame_->ShowTips();
	}
	for (auto frame : tech_frames_) {
		frame->ShowTips();
	}
}

