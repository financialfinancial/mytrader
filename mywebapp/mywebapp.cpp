/////////////////////////////////////////////////////////////////////////////
// Author: Steven Lamerton
// Copyright: (c) 2013 Steven Lamerton
// Licence: wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#include <wx/app.h>
#include <webview_chromium.h>

class MyApp : public wxApp
{
public:
    virtual bool OnInit();
    virtual int OnRun();
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    return true;
}

int MyApp::OnRun()
{
    return wxWebViewChromium::StartUpSubprocess();
}
